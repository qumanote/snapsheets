# Title: [Clear and concise issue title]

# Description:

A brief description of the issue or feature request.

# Type of Issue:

- [ ] Bug
- [ ] Feature Request
- [ ] Enhancement
- [ ] Documentation
- [ ] Breaking Change

# Details:

## Steps to Reproduce (For Bugs):

1. Step one
2. Step two
3. Step three

## Expected Behavior (For Bugs):

Describe what you expected to happen.

## Actual Behavior (For Bugs):

Describe what actually happened.

## Feature Description (For Feature Requests):

Provide a clear and detailed description of the feature you are requesting.

## Environment:

- Version: [Version number or branch name]
- OS: [Operating System]
- Python: [Python version]

## Additional Information:

Any other context or screenshots that might be helpful.

## Possible Solution (Optional):

If you have an idea of how to fix the bug or implement the feature, describe it here.

# Checklist:

- [ ] I have searched for similar issues.
- [ ] I have included a detailed description.
- [ ] I have provided all necessary information.

# Related Issues:

(Reference any related issues or pull requests here)
