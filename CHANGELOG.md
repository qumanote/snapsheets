## v1.1.2 (2024-10-05)

### Fix

- **snapsheets/downloader.py**: added response.raise_for_status
- **snapsheets/downloader.py**: set encoding explicitly
- **snapsheets/downloader.py**: fixed args for via_httpx
- **snapsheets/downloader.py**: fixed args of via_wget
- **snapsheets/downloader.py**: fixed args for via_requests
- **snapsheets/sheet.py**: fixed get_export_url to use parse.SplitResult
- **snapsheets/sheet.py**: changed import module: from urllib import parse
- **snapsheets/sheet.py**: fixed get_fmt. raise ValueError instead of sys.exit.
- **snapsheets/downloader.py**: added via_requests

### Notebooks

- **jupytext.toml**: enabled jupytext
- **notebooks/test_urlparse.ipynb**: added urlparse usage
- **mystmd/test_httpx.md**: httpxを使って確認した
- **mystmd/test_requests.md**: 確認内容を整理した
- **mystmd/**: added MyST md files

### Refactor

- **snapsheets/downloader.py**: use Path.write_text to save response text
- **snapsheets/sheet.py**: removed download_via_httpx
- **snapsheets/sheet.py**: removed download_via_wget

## v1.1.1 (2024-10-04)

### Fix

- **snapsheets/sheet.py**: added download_via_httpx
- **snapsheets/downloader.py**: added via_httpx
- **pyproject.toml**: added httpx: 0.27.2

## v1.1.0 (2024-10-02)

### Feat

- **snapsheets/downloader.py**: added downloader module

### Fix

- **sandbox/config.toml**: changed samples
- **sandbox/config.toml**: fixed config name
- **sandbox/config.toml**: fixed filename directory
- **sandbox/config.yaml**: fixed YAML samples
- **sandbox/snapd/.gitkeep**: removed fixed directory
- **snapsheets/book.py**: added error handlings
- **snapsheets/book.py**: changed config: desc -> description
- **snapsheets/book.py**: replaced sys.exit to raise ValueError
- **snapsheets/sheet.py**: removed unused module: subprocess

### Refactor

- **notebooks/config/**: remove unused/deprecated configurations
- **sandbox/config/**: removed unused configurations
- **snapsheets/book.py**: added loguru outputs
- **snapsheets/book.py**: fixed configuration loader

### CI and Hooks

- **.gitlab-ci.yml**: disabled build check on multiple Python
- **.pre-commit-config.yaml**: added hook: check-added-large-files
- **.pre-commit-config.yaml**: added hook: check-case-conflict
- **.pre-commit-config.yaml**: added hook: check-json
- **.pre-commit-config.yaml**: added hook: check-merge-conflict
- **.pre-commit-config.yaml**: added hook: check-toml
- **.pre-commit-config.yaml**: added hook: check-yaml
- **.pre-commit-config.yaml**: added hook: end-of-file-fixer
- **.pre-commit-config.yaml**: added hook: names-tests-test
- **.pre-commit-config.yaml**: added hook: pre-commit-hooks
- **.pre-commit-config.yaml**: added hook: ruff-format
- **.pre-commit-config.yaml**: added hook: trailing whitespace
- **.pre-commit-config.yaml**: updated hook: commitizen: 2.10.0 -> 3.29.0

## v1.0.0 (2024-09-17)

### BREAKING CHANGE

The configuration file format has changed.
Please update your existing configuration files to the new format to ensure compatibility.

### Feat

- **pyproject.toml**: changed main script name
- **snapsheets/v0/core.py**: moved to v0 (archive)
- **snapsheets/v1.py**: renamed next to v1

### Fix

- **snapsheets/book.py**: import tomllib
- **snapsheets/sheet.py**: replaced pendulum with datetime
- **snapsheets/v0/core.py**: import tomllib
- **snapsheets/v0/core.py**: replaced icecream with loguru
- **snapsheets/v0/core.py**: replaced icecream with print
- **snapsheets/v0/core.py**: replaced pendulum with datetime
- **snapsheets/v1.py**: fixed version info
- **tests/test_book.py**: fixed import v1
- **tests/test_next.py**: fixed import v1

### Refactor

- **docs/conf.py**: replaced pendulum with datetime
- **sandbox/config_**: removed deprecated config files
- **sandbox/config.toml**: fixed config example
- **snapsheets/__init__.py**: removed internal import
- **snapsheets/v0/__init__.py**: added as module (will be deprecated)

### Notebooks

- **notebooks/test_datetime.ipynb**: check datetime usage

### Dependencies

- **requirements.txt**: removed requirements.txt
- **pyproject.toml**: removed docopt (unused)
- **pyproject.toml**: removed icecream
- **pyproject.toml**: removed pendulum: 3.0.0
- **pyproject.toml**: removed toml: 0.10.2

## v0.7.2 (2024-09-15)

### Fix

- **.gitlab-ci.yml**: add test: poetry run snapsheets
- **snapsheets/book.py**: import sys

## v0.7.1 (2024-09-15)

### Fix

- **pyproject.toml**: add requests: 2.32.3

## v0.7.0 (2024-09-15)

### Feat

- **snapsheets/book.py**: split Book class
- **snapsheets/sheet.py**: add validate_url to validate the provided URL
- **snapsheets/sheet.py**: split Sheet class

### Fix

- **.gitlab-ci.yml**: add test coverage
- **snapsheets/next.py**: removed unused imports
- **snapsheets/sheet.py**: add check=True to subprocess.run
- **snapsheets/sheet.py**: add timeout for requests.get
- **snapsheets/sheet.py**: add URL validation at post_init
- **snapsheets/sheet.py**: add warning when not using HTTPS
- **snapsheets/sheet.py**: add early return
- **snapsheets/sheet.py**: fixed unshared condition
- **snapsheets/sheet.py**: moved subprocess.run in try-except block
- **snapsheets/sheet.py**: organized imports

### Refactor

- **sandbox/snapper.py**: removed old cli
- **snapsheets/config.py**: removed unused module
- **snapsheets/config.yml**: remove unused config
- **snapsheets/gsheet.py**: removed unused module
- **snapsheets/gsheet.yml**: remove unused config
- **snapsheets/log.py**: removed unused module
- **snapsheets/next.py**: created separate functions
- **snapsheets/sheet.py**: add self.parse_url

### Tests

- **tests/test_book.py**: add test for loading configurations from TOML format
- **tests/test_book.py**: add test for loading configurations from YAML format
- **tests/test_book.py**: embed configurations
- **tests/test_next.py**: add docstring to each test
- **tests/test_next.py**: add test for config options
- **tests/test_next.py**: add test for download method
- **tests/test_next.py**: add test for get_export_url method
- **tests/test_next.py**: add test for get_gid method
- **tests/test_next.py**: add test for get_key method
- **tests/test_next.py**: add test for mutually exclusive options
- **tests/test_next.py**: add test for next cli
- **tests/test_next.py**: add test for unshared Google sheet
- **tests/test_next.py**: add test for version
- **tests/test_next.py**: add test for process_config with mocked Book class
- **tests/test_next.py**: add test for process_url with mocked Sheet class
- **tests/test_next.py**: add test setup_parser with --config option
- **tests/test_next.py**: add test setup_parser with --url option
- **tests/test_sheet.py**: add test get_fmt method
- **tests/test_sheet.py**: fix assertion to match check=True
- **tests/test_sheet.py**: fixed mock value
- **tests/test_sheet.py**: add reqests.get mock
- **tests/test_sheet.py**: removed unused packages
- **tests/test_version.py**: removed test_version

### Documents

- **.gitignore**: ignore apidocs
- **.gitignore**: ignore assets
- **docs/conf.py**: add autodoc2 option
- **docs/Makefile**: add livehtml target

### Dependencies

- **pyproject.toml**: add pytest-cov: 5.0.0
- **pyproject.toml**: add pytest-html: 4.1.1
- **pyproject.toml**: add pytest-mock: 3.14.0
- **pyproject.toml**: added sphinx-autobuild: 2024.9.3
- **pyproject.toml**: added sphinx-autodoc2: 0.5.0
- **pyproject.toml**: added typer: 0.12.5
- **pyproject.toml**: dropped Python3.9

## v0.6.7 (2024-05-06)

### Fix

- **pyproject.toml**: upgraded pytest: 7.4.4 -> 8.2.0
- **pyproject.toml**: upgraded ruff: 0.1.15 -> 0.4.3
- **pyproject.toml**: upgraded pendulum: 2.1.2 -> 3.0.0
- **pyproject.toml**: dropped support for Python3.7 and 3.8

## v0.6.6 (2023-12-19)

### Fix

- **.gitlab-ci.yml**: Fixed GitLab CI
- **docs/conf.py**: Fixed E262 (inline comment should start with '# ')
- **notebooks/test_download.ipynb**: Testing with a notebook: downloader using urllib
- **pyproject.toml**: Enabled black -> Disabled (fails at GitLab CI pipline)
- **pyproject.toml**: Updated packages
- **snapper-cli.py**: Fixed F401 (module imported but unused)
- **snapsheets/config.py**: Add type: ignore

## v0.6.5 (2023-01-23)

### Fix

- **docs/conf.py**: GA4のIDに変更した

## v0.6.4 (2022-10-02)

### Fix

- **snapsheets/core.py**: removed logd
- **snapsheets/core.py**: disabled logd

## v0.6.3 (2022-10-01)

### Fix

- **snapsheets/next.py**: Show error message and set skip=true when the sheet URL might be unshared

## v0.6.2 (2022-06-15)

### Fix

- **snapsheets/next.py:load_config**: switch by suffix (TOML or YAML) of config file
- **snapsheets/next.py:load_config_yaml**: add support for config in YAML format


## v0.6.1 (2022-06-09)

### Fix

- **snapsheets/next.py:cli**: tidy up options
- **snapsheets/next.py:cli**: add debug option; switch logger display
- **snapsheets/next.py**: replaced ic with loguru.logger
- **snapsheets/core.py:cli**: fixed version option
- **snapsheets/core.py**: removed unused modules
- **snapsheets/core.py**: replaced ic with loguru.logger
- **snapsheets/core.py**: removed unused Logger class
- **snapsheets/core.py**: add deprecated messages
- **snapsheets/config.py**: disabled to load default config

### Refactor

- **snapsheets/gsheet.py**: removed unused import
- **snapsheets/config.py**: removed module
- **snapsheets/config.py**: removed unused functions
- **napsheets/config.py**: marked as removed

## v0.6.0 (2022-06-02)

### Feat

- **pyproject.toml**: added CLI named snapsheet-next
- **snapsheets/next.py:Book**: added Book class
- **snapsheets/next.py**: added module for next version

### Fix

- **sandbox/config.toml**: added new sample config file
- **snapsheets/next.py:Sheet**: removed default values
- **snapsheets/next.py:cli**: add CLI
- **snapsheets/next.py:Sheet**: enabled download and backup
- **snapsheets/next.py:cli**: added CLI
- **snapsheets/core.py:get_version**: removed function
- **snapsheets/core.py**: disabled icecream
- **pyproject.toml**: addeded pyproject.toml:version to version_files
- **setup.py**: removed setup.py

## v0.5.7 (2021-06-22)

### Fix

- **core.Book.add_sheet**: added skip flag; when sheet.skip = True
- **config/gsheet.toml**: added skip flag
- **core.Book.add_sheet**: changed where add sheet info shows
- **config/gsheet.toml**: fixed gsheet.toml
- **core.__version__**: fixed version number

## v0.5.6 (2021-05-10)

### Fix

- **core.py**: disabled debug comment

## v0.5.5 (2021-05-10)

### Fix

- **core.py:load_toml**: fixed load_toml
- **core.py:update_config**: fixed update_config

## v0.5.4 (2021-05-10)

### Fix

- **notebooks/test_core.ipynb**: updated test_core.ipynb
- **core.Config.load_toml**: fixed to load multiple sheet configurations
- **sample1.toml**: fixed a little
- **notebooks/config**: split config files

## v0.5.3 (2021-04-22)

### Feat

- **core.Book**: new function: core.Book.export_urls()
- **core.cli**: new optional argument: --config
- **core.Sheet**: new member: skip (default: false)

### Fix

- **snapper.py**: fixed snap5
- **core.Sheet**: will deprecated fname. use name instead
- **snapper.py**: added docstrings
- **snapper.py**: added function to show export_url()
- **core.Book.make_sheet**: added skip
- **core.Sheet**: fixed skip default value (True -> False)
- **core.Sheet**: added member skip:boolean = True (skip by default)
- **configs**: added symlinks to sandbox config directories
- **core.Config.get_config**: fixed to adapt both TOML/YAML format configuration
- **snapper.py**: added new function to test core.Config module
- **core.py**: fixed debug messages
- **sheets**: fixed sample files for sheets
- **configs**: renamed files
- **configs**: added new config files
- **core.Config.check_path**: added staticmethod
- **snapper.py**: added export url check

## v0.5.2 (2021-04-12)

### Fix

- bump: version 0.5.1 → 0.5.2
- **pyproject.toml**: fixed description

## v0.5.1 (2021-04-12)

### Fix

- **core.__version__**: fixed __version__ for now

## v0.5.0 (2021-04-12)

### Refactor

- removed CHANGELOG.md made by mistake

### Fix

- **core.Config**: changed default values
- **gsheet.py**: add deprecated messages
- **gsheet.py**: disabled logging
- **snapper.py**: fixed
- **core.py**: fixed get_config
- **snapper-cli.py**: URLを設定しない場合は、Bookクラスを使って一括ダウンロードできるようにした
- **config.toml**: fixed TOML configuration
- **config.toml**: 設定ファイルの書式を修正した
- **notebooks**: removed notebooks
- **test_config.yml**: removed test_config.yml
- **test_core.ipynb**: add new notebook
- **snapsheets.toml**: add TOML configuration
- **snapper-cli.py**: fixed argparse
- **snapper-cli.py**: CLIのパートを別のファイルに分離した
- **snapper.py**: CLIを準備中
- **snapper.py**: 設定を確認する関数を追加
- **snapper.py**: add argparse
- **config.toml**: removed config.toml

### Feat

- **core:get_version**: get version from pyproject.toml (use commitizen version number)
- **core.py**: add cli with argparse
- **core:cli**: add CLI tool (with no argparse implemented)
- **core.py**: add Book class
- **core.py**: tool.snapsheets.config の設定を優先するように変更した
- **core:cli**: add cli with argparse
- **core.py**: add new Logger class

## v0.4.1 (2021-04-07)

### Fix

- **snapper.py**: add docopt
- **config.py**: add deprecated messages
- **gsheet.py**: add deprecated messages
- **core.py**: add docopt
- **core.py**: add docopt (testing now)

## v0.4.0 (2021-04-07)

### Fix

- **snapper.py**: updated snap3 test
- **__init__.py**: add core modules
- **config.py**: improved Sheet class
- **config.py**: update Sheet class
- **test_config.ipynb**: add new test function for Config and Sheet dataclasses
- **config.py:load_config**: tried to add member variables
- **config.py:Sheet**: add new dataclass for single Google Sheet
- **config.py:Config**: add __post_init__ function
- **config.py**: icecream: number of config files loaded
- **config.py**: sorted sections

### Feat

- **core.Book**: add Book class (inherit from Sheet)
- **core.py**: collecting core functions

### Refactor

- **static**: removed hugo static
- **themes**: removed hugo themes

## v0.3.0 (2021-04-02)

### Fix

- **snapper.py**: fixed main functions
- **gsheet.toml**: add TOML format configuration file
- **snapper.py**: fixed snapper
- **gsheet.py**: fixed log messages in Gsheet
- **gsheet.py**: add method : Gsheet.snapshots()
- **gsheet.yml**: add "test2"
- **gsheet.yml**: add "desc"
- **gsheet.py**: add FileNotFoundError exception and log messages
- **sandbox/snapper.py**: add snap1()
- **sandbox/snapper**: add snapd directory
- **config.Config**: add deprecated messages
- **config.Config**: add methods for config.Config class
- **poetry.lock**: removed from git

### Feat

- **gsheet.py**: Update Gsheet class
- **sandbox/snapper.py**: add sandbox for testing
- **config.py**: add Config class

### Refactor

- **python**: removed : python
- **README.md**: merged python/README.md and README.md (temporarily)
- **CHANGELOG.md**: moved : python/CHANGELOG.md -> CHANGELOG.md
- **pyproject.toml**: moved : python/pyproject.toml -> pyproject.toml
- **setup.py**: moved : python/setup.py -> setup.py
- **notebooks**: moved : python/notebooks -> notebooks
- **tests**: moved : python/tests -> tests
- **snapsheets**: moved : python/snapsheets -> snapsheets
- **docs**: moved : python/docs -> docs

## v0.2.3 (2020-12-08)

### Fix

- **poetry**: add poetry.loc

## v0.2.0 (2020-05-26)

## v0.1.6 (2020-05-25)
