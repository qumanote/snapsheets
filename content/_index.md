# 使い方

```python
>>> import snapsheets as ss
>>> ss.add_config('config.yml')
>>> ss.get('test1', by='wget')
```

---

## 設定ファイル

- 設定ファイルは``YAML``形式で用意する

```yaml
volumes:
  snapd: 'data/'

test1:
  key: '1NbSH0rSCLkElG4UcNVuIhmg5EfjAk3t8TxiBERf6kBM'
  gid: 'None'
  format: 'xlsx'
  sheet_name:
    - 'シート1'
    - 'シート2'
  stem: 'test_sheet'
  datefmt: '%Y'
```

---

# インストール

- ``PyPI``からインストール

```bash
$ pip3 install snapsheets
$ pip3 show snapsheets
```

- ``Test PyPI``からインストール

```bash
$ pip3 install -i https://test.pypi.org/simple/ snapsheets
$ pip3 show snapsheets
```

---
