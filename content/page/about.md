---
title: About
subtitle: About this package
comments: false
date: "2020-04-29"
weight: 1
---

このパッケージを作成した経緯

<!--more-->
---

# Day 0

- ``Google Spreadsheet`` 大好き
- データ分析にもお手軽に使いたい
- でもAPIを使ってアクセスするのはめんどくさい

---

# Day 1

- サンプルとして使う``spreadsheet``を作成した
- ``wget``を使ってファイルを取得した

---

# Day 2

- パッケージ名を考える
- ``PyPI``で同じ名前がないか検索した

---

# Day 3

- モジュールを作成した

---

# Day 4

- ``setup.py``を作成した

---

# Day 5

- ``pytests``を作成した

---

# Day 6

- 休んだ

---

# Day 7

- パッケージが完成した
- ``Test PyPI``に登録した
