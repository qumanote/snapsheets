---
title: 設定ファイルの書き方
subtitle: config.yml
comments: false
date: "2020-05-09"
weight: 12
---

設定ファイルに書く内容をまとめた。

<!--more-->
---

## データの保存先の設定

```yaml
volumes:
   snapd: 'data'
```

## スプレッドシートの設定

```yaml
スクリプトで呼び出す変数名:
  key: 'スプレッドシートのID'
  gid: 'シートのID' or 'None'
  format: 'ダウンロード形式'
  sheet_name:
    - 'シート1'
    - 'シート2'
  stem: 'ダウンロードしたファイル名（のstem部分）'
  datefmt: 'バックアップしたファイルのprefixの日付フォーマット'
```

---

## 設定ファイルの具体例

- 1つの設定ファイルに複数のスプレッドシートの情報を書くことができる

```yaml
volumes:
  snapd: 'data'

test1:
  key: '1NbSH0rSCLkElG4UcNVuIhmg5EfjAk3t8TxiBERf6kBM'
  gid: 'None'
  format: 'xlsx'
  sheet_name:
    - 'シート1'
    - 'シート2'
  stem: 'test_sheet'
  datefmt: '%Y'

test2_ssd:
  key: '16Sc_UgShNuxMfRnBiFsjmfThE1VfVhJf3jgmxNvFeEI'
  gid: 0
  format: 'csv'
  sheet_name:
    - 'ssd'
  stem: 'storage_compare'
  datefmt: '%Y%m%d'
```

- ラベルを指定するだけで、スプレッドシートのスナップショットを取得できる
- データの保存先が存在しない場合はエラーがでる

```python3
import snapsheets as ss

ss.get('test1', by='wget')
# ==> PosixPath('data/2020_test_sheet.xlsx')

ss.get('test2_ssd', by='wget')
# ==> PosixPath('data/20200509_storage_compare.csv')
```
