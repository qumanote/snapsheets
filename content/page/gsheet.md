---
title: スプレッドシート
subtitle: 設定
comments: false
date: "2020-05-09"
weight: 11
---

テストに使えるサンプル用のスプレッドシートと、その使い方を用意した。


<!--more-->
---

# スプレッドシートの設定

- リンクの共有をONにする
- 「リンクを知っている全員が閲覧」できるモードにする

# スプレッドシートのエクスポート

スプレッドシートのエクスポートをブラウザ上で実行する場合

- ``[ファイル]`` → ``[ダウンロード]`` → ``(形式を選択)``

とメニューを操作する必要があるが、同じことをコマンドラインから実行できる。

``Wget``を使って実行する場合、必要な要素を抜き出すと以下のように打てばよい。

```bash
$ wget -O {FILENAME} 'https://docs.google.com/spreadsheets/d/{KEY}/export?format={FMT}&gid={GID}'
```

1. ``key`` : スプレッドシートの ID
1. ``gid`` : シートの ID
1. ``fmt`` : ダウンロード形式

の3要素が必要となる。
これはらは、元のURLを参照して、対応する文字列を抜き出す。


# サンプル1

ぱっと使えるように、誰でもアクセスできるサンプルファイルを用意した。
悪用しない範囲で使ってもらえると嬉しい。

- サンプル1 : https://docs.google.com/spreadsheets/d/1NbSH0rSCLkElG4UcNVuIhmg5EfjAk3t8TxiBERf6kBM/

```python3
key = '1NbSH0rSCLkElG4UcNVuIhmg5EfjAk3t8TxiBERf6kBM'
gid = 'None'
fmt = 'xlsx'

import snapsheets as ss
ss.fetch(key, gid, fmt, by='wget')
# ==> PosixPath('snapshot.xlsx')
```
