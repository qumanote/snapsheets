---
title: ローカルで開発したいひと
subtitle: git clone https://gitlab.com/shotakaha/snapsheets.git
comments: false
date: "2020-05-09"
weight: 200
---

ローカル環境で開発＆テストする場合は、``GitLab``のリポジトリからクローンする。

<!--more-->
---

# セットアップ

1. ``GitLab``リポジトリをクローンする
1. ``setup.py`` があるディレクトリに移動する
1. ``pip3 install .`` でローカル環境にインストールする

```bash
$ git clone https://gitlab.com/shotakaha/snapsheets.git
$ cd snapsheets/python/
$ pip3 install .
$ pip3 show snapsheets
```
