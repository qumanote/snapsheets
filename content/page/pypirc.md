---
title: pypirc
subtitle: $HOME/.pypirc
comments: false
date: "2020-05-09"
weight: 203
---


[PyPI](https://pypi.org/) と[Test PyPI](https://test.pypi.org/) のアカウント（API Token）情報は ``$HOME/.pypirc`` に書いておくことができる。

<!--more-->

```text
[distutils]
  index-servers=
    pypi
    testpypi

[testpypi]
  repository: https://test.pypi.org/legacy/
  username = __token__
  password = pypi-*****

#[pypi]
#  username = __token__
#  password = pypi-*****
```

このファイルは平文で保存しているので、本当は ``password`` とか書くのはよくないのだけれど、、、他に安全なやり方が見つからなかったので、しかたない。
とりあえず、ファイルのパーミッションは ``600`` に変更しておいた。

```bash
$ chmod 600 ~/.pypirc
```

## 参考記事

- https://truveris.github.io/articles/configuring-pypirc/
