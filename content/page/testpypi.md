---
title: Test PyPI に登録
subtitle: python3 -m twine upload --repository testpypi dist/*
comments: false
date: "2020-05-09"
weight: 202
---

パッケージをだれでも使えるように [PyPI](https://pypi.org/) に登録する前に、 [Test PyPI](https://test.pypi.org/) に登録してテストすることが必要。

- https://packaging.python.org/tutorials/packaging-projects/

<!--more-->
---

# Test PyPI のアカウント作成

- https://test.pypi.org/account/register/
- 名前、メールアドレス、ユーザ名などを登録
- メールアドレスの認証
- 二段階認証を有効にする
- API Token を発行する

## API Token の発行

- https://test.pypi.org/manage/account/#api-tokens

```text
pypi-たくさんの文字列
```

- ``Token name (required)`` : トークン名を入力
- ``Permissions`` : ``Upload packages``
- ``Scope (required)`` : ``Entire account (all projects)``

1. トークン名はなんでもOK（あとで自分が分かればよい）
1. ``Permissions``は他に選択肢がない
1. ``Scope``は、はじめて作ったためか、他に選択肢がなかった
1. ``Add token``をクリックし、トークンを発行する
1. 表示されたトークンをどこかにコピペする（``Copy token``ボタンがある）
1. トークンはあとで確認することができないので、どこかにコピペする
1. ``$HOME/.pypirc``というのがあるらしいので、使い方を調べる


## Token の使い方

```text
[pypi]
  username = __token__
  password = pypi-たくさんの文字列
```

# ``twine``をインストールする

```bash
$ pip3 install twine
```

1. パッケージをアップロードするのに``twine``が必要
1. 最近はこのパッケージを使うのが推奨されているらしい

# パッケージを作成する

```bash
$ python3 setup.py sdist bdist_wheel
running sdist
...（省略）
running bdist_wheel
...（省略）

$ ls dist/
snapsheets-0.1.1-py3-none-any.whl
snapsheets-0.1.1.tar.gz
```

1. ``setup.py``があるディレクトリで上記のコマンドを実行する
1. ``dist/``の中に``*.whl``と``*.tar.gz``が作成されていることを確認
1. すでにファイルがある場合は、削除しておくとよいかも


# ``Test PyPI``にパッケージを登録

```bash
$ twine upload --repository testpypi dist/*
Uploading distributions to https://test.pypi.org/legacy/
Enter your username: __token__  ## <- 文字通り __token__ と入力
Enter your password: ## pypi- からはじまるトークン文字列を入力
Uploading snapsheets-0.1.1-py3-none-any.whl
100%
Uploading snapsheets-0.1.1.tar.gz
100%
View at:
https://test.pypi.org/project/snapsheets/0.1.1/
```

## 登録時に出会ったエラー

```text
HTTPError: 400
Client Error: 'xxxxx' is an invalid value for Author-email.
Error: Use a valid email address
```

- メールアドレス（``author_email``）を``xxxxx``としていたので怒られた
- きちんとしたメールアドレスに変更した
  - ``Test PyPI``に登録したアドレス以外でもOK

---

```text
HTTPError: 400
Client Error: File already exists.
See https://test.pypi.org/help/#file-name-reuse
```

- 同じ名前のファイルを追加したら怒られた
- ``Test PypI``（と``PyPI``）は同じ名前のファイルは受け付けないシステムになっているらしい



# ``Test PyPI``からパッケージをインストール

```bash
$ pip3 install -i https://test.pypi.org/simple/ snapsheets
$ pip3 show snapsheets
```

``PyPI``への登録は、もう少しあとにしよう
