---
title: Usage
subtitle: 使い方
comments: false
date: "2020-05-09"
weight: 10
---

```python
>>> import snapsheets as ss
>>> ss.add_config('config.yml')
>>> ss.get('test1', by='wget')
```

1. このモジュールをインポートする（``import snapsheets as ss``）
1. 設定ファイルを読み込む（``ss.add_config``）
1. スプレッドシートを取得する（``ss.wget``）

<!--more-->
---

# 設定ファイルの例

- 保存先の設定

```yml
volumes:
  snapd: 'snapshots'
```

- スプレッドシート情報

```yaml
test_sheet:
  key: '1NbSH0rSCLkElG4UcNVuIhmg5EfjAk3t8TxiBERf6kBM'
  gid: 'None'
  format: 'xlsx'
  sheet_name:
    - 'シート1'
    - 'シート2'

YOUR_SHEET_LABEL:
  key: 'KEY_NUMBERS'
  gid: 'INTEGER_or_NONE'
  format: 'xlsx'
  sheet_name:
    - 'NAME_OF_SHEET_IN_YOUR_GSPREADSHEET'
```
