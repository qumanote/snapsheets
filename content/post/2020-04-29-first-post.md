---
title: GitLabにリポジトリを作成
date: 2020-04-28
---

GitLabにリポジトリを作成。
その際、``GitLab Pages/Hugo``のテンプレートを選択。
ドキュメントは``Hugo``を使って書く予定。
