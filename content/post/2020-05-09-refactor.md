---
title: けっこう書き換えた
subtitle: 引数の設定、難しい
date: 2020-05-09
---

いくつかの自分用スクリプトで使ってみて、使い回し方も分かってきたので、より使いやすくなるように書き換えた。

「とりあえず使ってみる」ことができるように、設定ファイルを作成しなくても使えるような関数を用意した。
引数をどういう風に与えるのがよいのか、けっこう悩んだ。

<!--more-->

# 書き換えた関数

1. ダウンロードする関数（``snapsheets.gsheet.fetch``）とバックアップを作成する関数（``snapsheets.gsheet.backup``）を再構成した
1. ``curl``コマンドも使えるようにした（実はまだ途中）


# ダウンロードする関数

```python
>>> import snapsheets as ss
>>> fname = ss.fetch(key, gid, fmt, by, stem='snapshot', snapd='.'):
```

## 必須

- ``key`` : Google spreadsheet の Key ID
- ``gid`` : Google spreadsheet の シートID
- ``fmt`` : ダウンロードの形式（``.xlsx``, ``.csv`` など）
- ``by`` : コマンドを指定（``wget`` or ``curl``）

## オプション

- ``stem`` : ダウンロードしたファイル名（のstem部分）
- ``snapd`` : ダウンロードしたファイルを保存するディレクトリ

## 備考

- ``key``, ``gid``, ``fmt`` は Google spreadsheet を指定するために必要
- ``by``はダウンロードするコマンドを指定するために追加した
- 返り値はダウンロードしたファイル名にした。こうしておけば、そのまま``backup``に渡したり、``pandas.read_csv / read_excel`` に渡したりできると考えたから

# バックアップを作成する関数

```python
>>> import snapsheets as ss
>>> fname = ss.backup(fname, datefmt='%Y%m%dT%H%M%S', stem=None, snapd='.')
```

## 必須

- ``fname`` : バックアップするファイル名

## オプション

- ``datefmt`` : バックアップしたファイルに付ける``prefix``の日付フォーマット
- ``stem`` : バックアップしたファイル名（のstem部分）
- ``snapd`` : バックアップしたファイルを保存するディレクトリ

## 備考

- ファイル名を与えれば、適当にバックアップを作成してくれるといいと思った
- なので、``stem = None``の場合は、``fname``のstemを利用する

```python3
from pathlib import Path

if not stem:
    stem = Path(fname).stem
```

- ``datefmt``は好きなように変更できるようにした
- 保存先も``snapd``で変更できるようにした
- 返り値はバックアップしたファイル名にした。こうしておけば、そのまま ``pandas.read_csv / read_excel`` に渡すことができると考えたから
