%.. snapsheets documentation master file, created by
%   sphinx-quickstart on Tue Jan 19 20:12:21 2021.
%   You can adapt this file completely to your liking, but it should at least
%   contain the root `toctree` directive.

# Snapsheets |version| documentation

Lazy downloader for Google Spreadsheets.

Isn't it tiresome to download one by one from the browser ?
This package enables you to download public sheet from your terminal.
This is great if you want to take snapshots periodically 🚀

> The sheet's sharing should be turned on.


```{toctree}
---
maxdepth: 1
---
install
usage/index
notes/index
```

## APIs

```{toctree}
---
maxdepth: 1
---
apidocs/index
```


## Indices and tables

* {ref}`genindex`
* {ref}`modindex`
* {ref}`search`
