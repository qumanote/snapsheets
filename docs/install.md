# Install

## From PyPI

```shell
$ pip3 install snapsheets
$ pip3 show snapsheets
```

## Using pipx

```console
$ pipx install snapsheets
```

## Using Poetry

```shell
$ poetry add snapsheets
```

## Using uv

```console
$ uv tool install snapsheets
```
