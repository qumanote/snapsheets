# Command Line Tool

- CLI is also available as of version ``0.5.0``
- Use ``argparse`` to parse arguments.
- Use ``loguru`` to output logs.
- `Book-mode` and `Sheet-mode` are mutually exclusive.

## Usage

```{code-block} console
Usage: snapsheets [-h] [--config config | --url url] [-o filename] [-d description] [-t format] [-v] [--skip]

Optional arguments:
    -h, --help       show this help message and exit
    --config config  set config file or directory.
    --url url        set URL of Google spreadsheet.
    -o filename      set output filename.
    -d description   set description of a spreadsheet.
    -t format        set datetime prefix for backup filename.
    -v, --version    show program's version number and exit
```

---

## Book-mode (with ``--config`` arguments)

- Load configuration from directory or files
- Download the sheets in bulk.
- ``--config`` option can be omitted.
  - Default filename is `./config.toml`
  - Default directory is `./config/`
- If the configuration directory does not exist, it will switch to the current directory.

```{code-block} console
$ snapsheets
2024-09-15T22:04:36 | ERROR    | URL might be unshared or inaccessible. Status: 401 - Unauthorized
2024-09-15T22:04:37 | SUCCESS  | 🤖 Downloaded as snapd/sample1.csv
2024-09-15T22:04:37 | SUCCESS  | 🚀 Renamed to snapd/20240915_sample1.csv
2024-09-15T22:04:38 | SUCCESS  | 🤖 Downloaded as snapd/sample2.csv
2024-09-15T22:04:38 | SUCCESS  | 🚀 Renamed to snapd/202409_sample2.csv
2024-09-15T22:04:39 | SUCCESS  | 🤖 Downloaded as snapd/sample3.csv
2024-09-15T22:04:39 | SUCCESS  | 🚀 Renamed to snapd/2024-09-15T22h04m39s_sample3.csv
```

- With ``--debug`` option

```{code-block} console
$ snapsheets --debug
2024-09-15T22:06:03 | INFO     | snapsheets.v1.cli:126 | Running NEXT version
2024-09-15T22:06:03 | INFO     | snapsheets.book.get_fnames:63 | Load config : config.toml
2024-09-15T22:06:05 | ERROR    | snapsheets.sheet.validate_url:111 | URL might be unshared or inaccessible. Status: 401 - Unauthorized
2024-09-15T22:06:05 | INFO     | snapsheets.sheet.snapshot:234 | 📣 Sample1: storage_comparison:SDカード (%Y%m%d)
2024-09-15T22:06:06 | SUCCESS  | snapsheets.sheet.download:206 | 🤖 Downloaded as snapd/sample1.csv
2024-09-15T22:06:06 | SUCCESS  | snapsheets.sheet.backup:230 | 🚀 Renamed to snapd/20240915_sample1.csv
2024-09-15T22:06:06 | INFO     | snapsheets.sheet.snapshot:234 | 📣 Sample2: storage_comparison:ssd (%Y%m)
2024-09-15T22:06:07 | SUCCESS  | snapsheets.sheet.download:206 | 🤖 Downloaded as snapd/sample2.csv
2024-09-15T22:06:07 | SUCCESS  | snapsheets.sheet.backup:230 | 🚀 Renamed to snapd/202409_sample2.csv
2024-09-15T22:06:07 | INFO     | snapsheets.sheet.snapshot:234 | 📣 Sample3: snapsheet_sample_shared:シート2 (%Y-%m-%dT%Hh%Mm%Ss)
2024-09-15T22:06:09 | SUCCESS  | snapsheets.sheet.download:206 | 🤖 Downloaded as snapd/sample3.csv
2024-09-15T22:06:09 | SUCCESS  | snapsheets.sheet.backup:230 | 🚀 Renamed to snapd/2024-09-15T22h06m09s_sample3.csv
2024-09-15T22:06:09 | INFO     | snapsheets.sheet.snapshot:234 | 📣 Sample4: snapsheet_sample_unshared:シート2 (%Y%m)
2024-09-15T22:06:09 | INFO     | snapsheets.sheet.download:196 | Skipped downloading snapd/sample4_unshared.csv.
2024-09-15T22:06:09 | INFO     | snapsheets.sheet.backup:220 | Skipped renaming snapd/sample4_unshared.csv
```

---

## `Sheet-mode` (with ``--url`` arguments)

- Copy and paste an URL of the Google Spreadsheet.
- Downloaded to current directory.
- The default file name is ``DATETIME_snapshot.xlsx``.


```{code-block} console
$ snapsheets --url "URL"
2024-09-15T22:10:24 | SUCCESS  | 🤖 Downloaded as snapshot.csv
2024-09-15T22:10:24 | SUCCESS  | 🚀 Renamed to _snapshot.csv
```

- With ``--debug`` option

```{code-block} console
$ snapsheets --url "URL" --debug
2024-09-15T22:10:43 | INFO     | snapsheets.v1.cli:126 | Running NEXT version
2024-09-15T22:10:44 | INFO     | snapsheets.sheet.snapshot:234 | 📣 Add description here.
2024-09-15T22:10:45 | SUCCESS  | snapsheets.sheet.download:206 | 🤖 Downloaded as snapshot.csv
2024-09-15T22:10:45 | SUCCESS  | snapsheets.sheet.backup:230 | 🚀 Renamed to _snapshot.csv
```

## Settings in ``poetry``

- Added lines below to ``pyproject.toml``

```{code-block} toml
[tool.poetry.scripts]
snapsheets-v0 = "snapsheets.v0.core:cli"
snapsheets = "snapsheets.v1:cli"
```
