# URL format

## Browser URL

`https://docs.google.com/spreadsheets/d/{KEY}/edit?gid={GID}#gid={GID}`

## Export URL

`https://docs.google.com/spreadsheets/d/{KEY}/export?gid={GID}&format={FORMAT}`

---

## Downloadable format

### CSV: current sheet

`https://docs.google.com/spreadsheets/d/{KEY}/export?gid={GID}&format=csv`

### TSV: current sheet

`https://docs.google.com/spreadsheets/d/{KEY}/export?gid={GID}&format=tsv`

### Microsoft Excel: all sheets

`https://docs.google.com/spreadsheets/d/{KEY}/export?gid={GID}&format=xlsx`

### OpenDocument: all sheets

`https://docs.google.com/spreadsheets/d/{KEY}/export?gid={GID}&format=ods`
