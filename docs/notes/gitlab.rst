==================================================
GitLab repository
==================================================


How to clone
==================================================

.. code-block:: shell

    $ git@gitlab.com:shotakaha/snapsheets.git python-snapsheets
    $ cd python-snapsheets
    [master] $ poetry install


How to start ``poetry`` venv
==================================================

.. code-block:: shell

    [master] $ poetry shell
    (.venv) [master] $ code .


How to checkout issue branch
==================================================

#. GitLab上で ``issue`` を登録する
#. そのイシューを使ってブランチを作成する（ ``master`` から作成）
#. ローカルでコードの修正を行いコミットを作成する（ ``commitizen`` を使う）
#. リモートリポジトリにプッシュする
#. GitLab上でマージリクエストを作成する

.. code-block:: shell

    [master] $ git co 10-FIX-ISSUE
    (.venv) [10-FIX-ISSUE] $ code .
    (.venv) [10-FIX-ISSUE] $ cz c
    (.venv) [10-FIX-ISSUE] $ git push


How to bump up version
==================================================

#. ``cz bump -ch --dry-run`` でアップデート先のバージョン番号を確認する
#. ``pyproject.toml`` のバージョン（ ``[tool.poetry.version]`` ）を変更してコミットする
#. ``cz bump -ch`` でタグを作成する

.. code-block:: shell

    $ cz bump -ch --dry-run
    bump: version 0.4.1 → 0.5.0
    tag to create: v0.5.0
    increment detected: MINOR

    $ code pyproject.toml
    $ cz c

    $ cz bump -ch
    bump: version 0.4.1 → 0.5.0
    tag to create: v0.5.0
    increment detected: MINOR

    $ git push origin --tags
