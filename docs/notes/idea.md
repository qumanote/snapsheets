# Basic idea

僕はデータの整理にGoogleスプレッドシートをよく利用しています。
そのデータの可視化に``pandas``や``plotly``を使うことが多く、毎回CSV形式でダウンロードしてから読み込ませています。

ブラウザからぽちぽちしてダウンロードするのはめんどうだなぁと感じていたため、コマンドラインツールで実行できる方法を探していたら、StackOverflowで ``wget`` する方法がありました。


[Download unpublished Google spreadsheet as CSV](https://stackoverflow.com/questions/10730712/download-unpublished-google-spreadsheet-as-csv)

```bash
$ wget "https://docs.google.com/spreadsheets/d/<KEY>/export?gid=<GID>&format=csv"
```

これは、ダウンロードしたシートの``<KEY>``（=ブックのID）と``<GID>``（=シートのID）を使って``wget``しています。
これで当初の目的は達成できたのですが、毎回スプレッドシートの``KEY``と``GID``を抜きだして並べるのは面倒です。
スプレッドシートのURLをコピペして引数に渡すだけで、ダウンロードできるようになったら便利だろうなと考え、Pythonパッケージを作ってみることにしました。

```{note}
このパッケージを作成しはじめたのは2020年のGWころで、ちょうどコロナ禍でステイホームしていたときです
```

## What I wanted to do

```bash
$ snapsheets シートのURL
==>（シートのURLからKEYとGIDを抽出）
==>（CSV形式でダウンロード）
snapsheet_KEY_GID.csv
```

### シートのURL

Googleスプレッドシートの共有設定が``リンクを知っている全員``になっているのを前提としています（役割は``閲覧者``でOK）。
コピーのしかたによってURLは2種類の形式があります。

1. 共有設定から``リンクのコピー``をした場合 : ``https://docs.google.com/spreadsheets/d/KEY/edit?usp=sharing``
1. 開いているシートのURLを直接コピーした場合 : ``https://docs.google.com/spreadsheets/d/KEY/edit?gid=GID``

```{note}
共有されていない or 共有設定が``制限付き``の場合は、「ダウンロードできませんでした。共有設定を確認してください。」のようなエラーメッセージが出せるようにしたいです。
```

## 設定ファイルに必要なアイテム

- ``URL`` : ブラウザからコピペしたURL（``KEY`` と ``GID`` はURLから抽出すればOK）
- ``FMT`` : ダウンロードの形式（``csv``, ``tsv``, ``xlsx``, ``ods`` に限定する）
- ``FILENAME`` : ダウンロードしたファイルのファイル名（拡張子は除く）
