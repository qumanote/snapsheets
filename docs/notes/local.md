# Local Test

## Install to local environment

Use `poetry` for local development

```console
$ poetry install
Installing dependencies from lock file
Package operations: 0 installs, 0 updates, 0 removals
Installing the current project: snapsheets (0.6.0)

$ which snapsheets
~/Library/Caches/pypoetry/virtualenvs/snapsheets-mU0WpA-6-py3.10/bin/snapsheets

$ cd sandbox
$ snapsheets

$ which snapsheets-next
~/Library/Caches/pypoetry/virtualenvs/snapsheets-mU0WpA-6-py3.10/bin/snapsheets-next

$ cd sandbox
$ snapsheets-next
```

## Unit Test

Use `pytest` to run unit test

```console
$ poetry run pytest --verbose

============================= test session starts ==============================
platform darwin -- Python 3.12.6, pytest-8.3.3, pluggy-1.5.0
cachedir: .pytest_cache
rootdir: ~/repos/gitlab.com/qumasan/snapsheets
configfile: pyproject.toml
plugins: cov-5.0.0, html-4.1.1, metadata-3.1.1, anyio-4.4.0, mock-3.14.0
collecting ... collected 8 items

tests/test_book.py::test_load_config_from_toml PASSED                    [ 12%]
tests/test_book.py::test_load_config_from_yaml PASSED                    [ 25%]
tests/test_sheet.py::test_get_fmt PASSED                                 [ 37%]
tests/test_sheet.py::test_get_key PASSED                                 [ 50%]
tests/test_sheet.py::test_get_gid PASSED                                 [ 62%]
tests/test_sheet.py::test_get_export_url PASSED                          [ 75%]
tests/test_sheet.py::test_unshared_google_sheets PASSED                  [ 87%]
tests/test_sheet.py::test_download PASSED                                [100%]
======================== 8 passed, 1 warning in 11.04s =========================
```

- Use `pytest-html` to save unittest result as html
- Use `pytest-cov` to show test coverage

```console
$ poetry run pytest --verbose --cov=snapsheets --html=report.html

---------- coverage: platform darwin, python 3.12.6-final-0 ----------
Name                     Stmts   Miss  Cover
--------------------------------------------
snapsheets/__init__.py       2      0   100%
snapsheets/book.py          65     32    51%
snapsheets/core.py         327    249    24%
snapsheets/next.py          35     25    29%
snapsheets/sheet.py         86     19    78%
--------------------------------------------
TOTAL                      515    325    37%

- Generated html report: ./report.html -
```
