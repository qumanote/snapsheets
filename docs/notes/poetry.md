# Publish to PyPI using ``poetry``

## Publish to PyPI

```bash
# build package
$ poetry build
Building snapsheets (0.6.0)
- Building sdist
- Built snapsheets-0.6.0.tar.gz
- Building wheel
- Built snapsheets-0.6.0-py3-none-any.whl

# publish to TestPyPI
$ poetry publish -r testpypi

# publish to PyPI
$ poetry publish
Publishing snapsheets (0.6.0) to PyPI
- Uploading snapsheets-0.6.0-py3-none-any.whl 100%
- Uploading snapsheets-0.6.0.tar.gz 100%
```

## 設定したときのメモ

### ``poetry`` を使って既存のプロジェクトを初期化

```bash
$ poetry init
```

### プロジェクトの基本情報を入力

```bash
This command will guide you through creating your pyproject.toml config.

Package name [python]:  snapsheets
Version [0.1.0]:  0.2.2
Description []:  Wget snapshots of google sheets
Author [Shota Takahashii <shotakaha+py@gmail>, n to skip]:  shotakaha <shotakaha+py@gmail.com>
License []:  MIT
Compatible Python versions [^3.9]:
```

### 依存パッケージ（ ``dependencies`` ）を追加

```bash
$ poetry add pendulum
$ poetry add requests
$ poetry add loguru
```

### 開発依存パッケージ（ ``dev-dependencies`` ）を追加

```bash
$ poetry add --group dev ipykernel
$ poetry add --group test pytest
$ poetry add --group docs sphinx sphinx_rtd_theme myst_parser
```

##  ``TestPyPI`` の設定

```bash
$ poetry config repositories.testpypi https://test.pypi.org/legacy/
$ poetry config pypi-token.testpypi "TestPyPIのAPIトークン"
```

- TestPyPIのウェブサイトでトークンを再発行した
- トークンは``pypi-``で始まる文字列

## ``PyPI`` の設定

```bash
$ poetry config pypi-token.pypi "PyPIのAPI Token"
```

- リポジトリはデフォルトで登録されている
- PyPIのウェブサイトでトークンを再発行した
- トークンは``pypi-``で始まる文字列
