# PyPI/TestPyPI

1. Get an account for TestPyPI and PyPI (respectively)
1. Publish packages to [Test PyPI](https://test.pypi.org)
1. Publish packages to [PyPI](https://pypi.org)

## Project page

- [snapsheets - Test PyPI](https://test.pypi.org/project/snapsheets/)
- [snapsheets - PyPI](https://pypi.org/project/snapsheets/)

## Preparation

- Follow the instruction at [Packaging Project](https://packaging.python.org/tutorials/packaging-projects/)
- That's all 🚀

## Note

- 同じバージョンをアップロードすることはできない
  - https://test.pypi.org/help/#file-name-reuse を参照
  - 動かないバージョンをあげても上書きして修正することができないので気をつける
  - ``twine upload`` する前に ``dist/*`` を空っぽにしたほうがよい
- メールアドレス（ ``setup.py`` に書く ``author_email`` ）は、 ``TestPyPI`` に登録したメールアドレスでなくてもOKだった
  - validな文字列であればなんでもよいみたい
  - invalidなアドレスは ``twine`` でエラーがでる（ ``xxxxxx`` にしてたら怒られた）
- ページの ``Project description`` が表示されてなかったり、メタデータの修正が必要な場合があるので、まずは ``TestPyPI`` でテストしてから ``PyPI`` にリリースするとよさげ
  - ``PyPI`` のアップロード手順がしっかりしてきたら ``TestPyPI`` はもう使わなくていいかも
