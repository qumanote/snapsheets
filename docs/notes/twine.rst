==================================================
Publish to PyPI with ``twine``
==================================================

Publish to PyPI
==================================================

#. 過去のビルドを削除する（ ``rm dist/*``）
#. ビルドする（``python3 setup.py``）
#. アップロード（``twine upload``）

.. code-block:: shell

  $ rm dist/*
  $ python3 setup.py sdist bdist_wheel

  $ twine upload dist/*
  Uploading distributions to https://upload.pypi.org/legacy/
  Uploading snapsheets-0.2.0-py3-none-any.whl
  Uploading snapsheets-0.2.0.tar.gz
  View at:
  https://pypi.org/project/snapsheets/0.2.0/


Settings at ``$HOME/.pypirc``
==================================================

- ``~/.pypirc`` に ``TestPyPI`` と ``PyPI`` のアカウント情報を追記する
- 以下の記事を参照
  - https://truveris.github.io/articles/configuring-pypirc/


.. code-block:: text

  [distutils]
    index-servers=
      pypi
      testpypi

  [testpypi]
    repository: https://test.pypi.org/legacy/
    username = __token__
    password = pypi-****

  [pypi]
    username = __token__
    password = pypi-****
