# Run on Terminal

- ターミナルから直接ダウンロードする場合は、次のコマンド（とオプション）でダウンロードできます

## wget

```console
$ wget "URL" -O FILENAME
$ wget "URL" --output-document FILENAME --quiet
```

- ``-q, --quiet``: quiet (no output)
- ``-O, --output-document=FILE``: write documents to FILE

## curl

```console
$ curl -L "URL" -o FILENAME
```

- ``-L, --location``:
- ``-o, --output``:

## httpx

```console
$ pipx install "httpx[cli]"
$ httpx "URL" --download FILENAME --follow-redirects
```

- `httpx` is modern HTTP client for Python3

:::{note}
`httpx` can be installed using pipx or uv.

```console
$ pipx install "httpx[cli]"
```

```console
$ uv tool install "httpx[cli]"
```

:::
