---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3.9.13 ('snapsheets-mU0WpA-6-py3.9')
  language: python
  name: python3
---

``Book``クラスを再定義する

- ``Book``は``Sheet``の集まり
- ``Sheet``の情報は設定ファイルに書いておく

```{code-cell} ipython3
from dataclasses import dataclass
from pathlib import Path
import tomli
from snapsheets.next import Sheet


@dataclass
class Book:
    fname: str = "config.toml"

    def __post_init__(self) -> None:
        p = Path(self.fname)
        if not p.exists():
            error = f"Unable to locate config file/directory. Perhaps you need to create a new config / file/directory. : {p}"
            ic(error)
            sys.exit()

        self.fnames = self.get_fnames()
        self.config = self.load_config()
        self.sheets = self.get_sheets()

    def get_fnames(self) -> list[Path]:
        """
        Get list of config files.

        Returns
        -------
        list[Path]
            list of config files
        """
        p = Path(self.fname)
        if p.is_file():
            return [p]
        fnames = sorted(p.glob("*.toml"))
        return fnames

    def load_config(self) -> dict:
        """Load configurations.

        Returns
        -------
        dict
            configuration in dict-object
        """
        config = {}
        for fname in self.fnames:
            with fname.open("rb") as f:
                _config = tomli.load(f)
                config.update(_config)
        return config

    def get_sheets(self) -> list[Sheet]:
        """
        Get list of sheets in configuration.

        Returns
        -------
        list[Sheet]
            list of Sheet objects
        """
        sheets = self.config.get("sheets")
        if sheets is None:
            return []

        sheets = []
        for sheet in self.config["sheets"]:
            url = sheet.get("url")
            filename = sheet.get("filename")
            desc = sheet.get("desc")
            datefmt = sheet.get("datefmt")
            skip = sheet.get("skip")
            _sheet = Sheet(
                url=url, filename=filename, description=desc, datefmt=datefmt, skip=skip
            )
            sheets.append(_sheet)
        return sheets

    def snapshots(self) -> None:
        """
        Take a snapshot of sheets.
        """

        for sheet in self.sheets:
            sheet.snapshot()
```

```{code-cell} ipython3
book = Book("config/next.toml")
```

```{code-cell} ipython3
book.fnames
book.config
book.sheets
book.snapshots()
```

``snapsheets.next.Book``クラスを読み込む``

```{code-cell} ipython3
from snapsheets.next import Book
```

```{code-cell} ipython3
book = Book("config/next.toml")
```

```{code-cell} ipython3
book.fnames
book.config
book.sheets
book.snapshots()
```

```{code-cell} ipython3

```
