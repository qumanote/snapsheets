---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3.9.13 ('snapsheets-mU0WpA-6-py3.9')
  language: python
  name: python3
---

``Config``クラスを再設計する

- ``snapsheets.config.Config`` モジュールを再設計する
- デフォルトの設定ファイルは ``config.toml`` にする
- ``config.toml`` が存在しない場合は ``config/*.toml`` を探してみる
- 設定ファイルが見つからない場合は、エラーメッセージを表示して終了する
- 再定義した ``Sheet``クラス（``snapsheets.next``）を使う

---

- テスト用の設定ファイル（``./config/sample1.toml`` or ``./config/sample2.toml``）を読み込む

```python
c = Config("config/sample1.toml")
c.fnames  # configファイルの一覧を表示
c.config  # 設定内容を表示（辞書型）
c.sheets  # Sheetオブジェクトの一覧を表示
```

---

- ``ss.config.volumes()`` の動作確認
- ``ss.config.sheets()`` の動作確認
- ``ss.config.sheet(name)`` の動作確認
- ``ss.config.options()`` の動作確認

これらは ``pytest`` でテストできるよう ``test_config.yml`` にまとめた

```{code-cell} ipython3
from icecream import ic
import tomli
from snapsheets.next import Sheet
```

```{code-cell} ipython3
# from urllib.parse import urlparse
# from pathlib import Path
from dataclasses import dataclass


@dataclass
class Config:
    fname: str = "config.toml"

    def __post_init__(self):
        p = Path(self.fname)
        if not p.exists():
            error = f"Unable to locate config file or config directory. Perhaps you need to create a new file/directory. : {p}"
            ic(error)
            sys.exit()

        self.fnames = self.get_fnames()
        self.config = self.load_config()
        self.sheets = self.get_sheets()

    def get_fnames(self):
        p = Path(self.fname)
        print(p)
        if p.is_file():
            return [p]

        fnames = sorted(p.glob("*.toml"))
        return fnames

    def load_config(self):
        config = {}
        for fname in self.fnames:
            with fname.open("rb") as f:
                _config = tomli.load(f)
                config.update(_config)
        return config

    def get_sheets(self):
        sheets = []
        for sheet in self.config.get("sheets"):
            name = sheet.get("name")
            url = sheet.get("url")
            skip = sheet.get("skip")
            desc = sheet.get("desc")
            datefmt = sheet.get("datefmt")
            _sheet = Sheet(
                url=url, filename=name, description=desc, datefmt=datefmt, skip=skip
            )
            sheets.append(_sheet)
        return sheets
```

```{code-cell} ipython3

```

```{code-cell} ipython3
c = Config("config/sample2.toml")
c.fnames
c.config
c.sheets
```

```{code-cell} ipython3
c.sheets[0].key
c.sheets[0].gid
c.sheets[0].export_url
c.sheets[0].snapshot()
```

```{code-cell} ipython3

```

# ``Config``クラス

- 初期化に必要な変数
  - `path` : これまで使っている変数（will be deprecated）
  - `confd` : 設定ファイルがあるディレクトリ（ ``default = "." ``）
  - `saved` : ダウンロードしたファイルを保存するディレクトリ（ ``default = "."`` ）
- `Config.get_fnames(fmt)` で設定ファイルの一覧を取得する
- `Config.load_config()` で設定したディレクトリにある設定ファイルを読み込む（辞書型になる）
  - `Config.load_yaml()` で `YAML` 形式のファイルを読み込む
  - `Config.load_toml()` で `TOML` 形式のファイルを読み込む（未テスト）

```{code-cell} ipython3
# sample_confd = "../snapsheets/"
sample_confd = "./config/"
sample = ss.config.Config(confd=sample_confd)
sample
```

``confd`` にある設定ファイルをロードする

```{code-cell} ipython3
cfg = sample.load_config()
# cfg
```

設定ファイルのセクション一覧を表示する

```{code-cell} ipython3
sample.sections()
```

保存先のディレクトリ一覧を表示する

```{code-cell} ipython3
sample.volumes
```

オプション一覧を表示する

```{code-cell} ipython3
sample.options
```

日付フォーマットの確認

```{code-cell} ipython3
sample.datefmt
```

スプレッドシートの一覧を表示する

```{code-cell} ipython3
sample.sheets()
```

シート名の一覧を表示する

```{code-cell} ipython3
sample.sheet_names()
```

``Sheet``クラスの再設計

- 単一シートのためのクラス
- ``url`` : ブラウザからコピペしたリンクを想定
- ``fmt`` : シートをダウンロードする時の形式。まずデフォルトを ``.xlsx`` 形式にする
  - 他の形式も使えるようにする（ ``.pdf`` などなど）
  - ``.csv``形式でダウンロードする場合は ``gid`` の情報が必要

---

  - どのシートID（``gid``）が付与されたURLでもOKなようにする
- ``key`` : 設定した ``url`` から ``key`` を抜き出す

- ``fname`` : ダウンロードファイルのファイル名（拡張子は不要）``stem``
- ``datefmt`` : ファイルをバックアップする際に追加する日付フォーマット（デフォルトは ``datefmt`` と同じにする）
- ``gid`` : シートID（リスト型にする？）
  - 最初のシートは ``gid=0`` だが、その他のシートはランダムで数字が割り振られる


# シートのサンプル

- シート1 : https://docs.google.com/spreadsheets/d/1NbSH0rSCLkElG4UcNVuIhmg5EfjAk3t8TxiBERf6kBM/edit#gid=0
- シート2 : https://docs.google.com/spreadsheets/d/1NbSH0rSCLkElG4UcNVuIhmg5EfjAk3t8TxiBERf6kBM/edit#gid=1772074296

```{code-cell} ipython3
url_sheet1 = "https://docs.google.com/spreadsheets/d/1NbSH0rSCLkElG4UcNVuIhmg5EfjAk3t8TxiBERf6kBM/edit#gid=0"
url_key1 = "1NbSH0rSCLkElG4UcNVuIhmg5EfjAk3t8TxiBERf6kBM"
url_sheet2 = "https://docs.google.com/spreadsheets/d/1NbSH0rSCLkElG4UcNVuIhmg5EfjAk3t8TxiBERf6kBM/edit#gid=1772074296"
```

```{code-cell} ipython3
s1 = ss.config.Sheet(url=url_sheet1)
s1
```

```{code-cell} ipython3
s2 = ss.config.Sheet(url=url_sheet2)
s2
```

```{code-cell} ipython3
k1 = ss.config.Sheet(url=url_key1)
k1
```

```{code-cell} ipython3
k1.gid
```

---

+++

# ``v0.2.2`` 以前 : 設定を読み込む

- ``Config`` クラスを用意してなかった
- ``snapsheets.config.add_config`` を使って設定ファイルを読み込む形式

+++

設定ファイルを読み込む（will be deprecated)

```{code-cell} ipython3
_ = ss.add_config("test_config.yml")
```

メソッドを確認

```{code-cell} ipython3
dir(ss.config)
```

## ``volumes`` 一覧

```{code-cell} ipython3
ss.config.volumes()
```

## ``sheets``一覧

```{code-cell} ipython3
ss.config.sheets()
```

## ``sheet``情報を表示

```{code-cell} ipython3
ss.config.sheet("test1")
```

```{code-cell} ipython3
ss.config.sheet("test2")
```

## ``options``一覧

```{code-cell} ipython3
ss.config.options()
```

```{code-cell} ipython3
ss.config.options().get("wget")
```

```{code-cell} ipython3
ss.config.options().get("curl")  ## None
```
