---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3.9.4 64-bit
  name: python39264bitcfa02c05961543a7a36e9f05df3c4e9a
---

# ``snapsheets.core`` モジュールのテスト

- ``Config``クラス
- ``Sheet``クラス
- ``Book``クラス

```{code-cell} ipython3
import os
import sys

sys.path.insert(0, os.path.abspath(".."))
```

```{code-cell} ipython3
import snapsheets as ss
import inspect
```

モジュール内で定義してあるクラス一覧

```{code-cell} ipython3
inspect.getmembers(ss, inspect.isclass)
```

# ``Config``

- ``__post_init__``でパスの存在を確認している
- デフォルト（だったり、引数で与えた）ディレクトリが存在しない場合は、カレントディレクトリを設定する
- 初期設定できる値
  - ``confd``
  - ``saved``
  - ``logd``
  - ``logf``
  - ``size``
  - ``backups``

+++

## 引数なしの場合

- ``notebooks/config`` : 作成してある
- ``notebooks/snapd`` : なし

```{code-cell} ipython3
cfg = ss.core.Config()
cfg
```

設定ファイルのセクション

```{code-cell} ipython3
cfg.sections()
```

日付フォーマットの確認

```{code-cell} ipython3
cfg.datefmt()
```

```{code-cell} ipython3
cfg.options()
```

```{code-cell} ipython3
cfg.sheet_names()
```

```{code-cell} ipython3
cfg.sheets()
```

# ``Sheet``

- ``Config``を継承している
- URL or KEY を設定してない場合、 ``warning`` を出すようにする

```{code-cell} ipython3
sheet = ss.Sheet()
```

``URL``を与えた場合

```{code-cell} ipython3
url = "https://docs.google.com/spreadsheets/d/1NbSH0rSCLkElG4UcNVuIhmg5EfjAk3t8TxiBERf6kBM/edit#gid=0"
sheet = ss.Sheet(url=url)
```

```{code-cell} ipython3
# sheet.snapshot()
```

```{code-cell} ipython3
sheet
```

# ``Book``

- ``Sheet`` を束ねるクラス
- 設定ファイルからスプレッドシートの情報を読み込む

```{code-cell} ipython3
book = ss.Book()
```

```{code-cell} ipython3
book
```

```{code-cell} ipython3
book.sheets
```

```{code-cell} ipython3
book.snapshots()
```

```{code-cell} ipython3

```
