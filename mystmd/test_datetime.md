---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: .venv
  language: python
  name: python3
---

```{code-cell} ipython3
from datetime import datetime
```

ローカルタイム（tzなし）を取得する

```{code-cell} ipython3
now = datetime.now()
now
```

ローカルタイム（tzあり）を取得する

```{code-cell} ipython3
now = datetime.now().astimezone()
now
```

現在の「年」を取得する

```{code-cell} ipython3
now.year
```

日付フォーマット（`strftime`）する

```{code-cell} ipython3
now.strftime("%Y%m%d")
```

```{code-cell} ipython3
now.strftime("%Y-%m-%dT%H:%M:%S")
```

```{code-cell} ipython3
now.strftime("%Y-%m-%dT%H:%M:%S%z")
```

```{code-cell} ipython3
now.strftime("%Y-%m-%dT%Hh%Mm%Ss")
```

```{code-cell} ipython3

```
