---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3.9.13 ('snapsheets-mU0WpA-6-py3.9')
  language: python
  name: python3
---

スプレッドシートのダウンロードを ``wget`` から ``urllib.request`` に置き換える

```{code-cell} ipython3
from snapsheets import next
```

サンプル用URLを取得する

- URL : "https://docs.google.com/spreadsheets/d/16Sc_UgShNuxMfRnBiFsjmfThE1VfVhJf3jgmxNvFeEI/edit#gid=0"

```{code-cell} ipython3
book = next.Book("config/next.toml")
sheet = book.sheets[1]
export_url = sheet.export_url
export_url
```

```{code-cell} ipython3
# sheet.download()
```

``urllib.request`` を使って ``export_url`` にアクセスする

- レスポンス（``http.client.HTTPResponse``）に対して ``read`` することで内容を取得することができる
- 限定公開の場合、レスポンスが0
- 公開されている場合、レスポンスあり（バイナリーデータ）

```{code-cell} ipython3
import urllib.request
```

リンクが公開されている場合

```
b'\xe7\xa8\xae\xe9\xa1\x9e,\xe3\x83\xa1\xe3\x83\xbc\xe3\x82\xab\xe3\x83\xbc,\xe8\xa8\x98\xe5\x8f\xb7,\xe3\x82\xb7\xe3\x83\xaa\xe3\x83\xbc\xe3\x82\xba,\xe5\xae\xb9\xe9\x87\x8f\xef\xbc\x88GB\xef\xbc\x89,\xe5\x9e\x8b\xe7\x95\xaa,\xe4\xbe\xa1\xe6\xa0\xbc,\xe7\xa2\xba\xe8\xaa\x8d\xe6\x97\xa5,Amazon\xe3\x81\xaeURL,\xe8\xaa\xad\xe5\x87\xba\xe9\x80\x9f\xe5\xba\xa6\xef\xbc\x88MB/s\xef\xbc\x89,\xe6\x9b\xb8\xe8\xbe\xbc\xe9\x80\x9f\xe5\xba\xa6\xef\xbc\x88MB/s\xef\xbc\x89,\xe3\x82\xb9\xe3\x83\x94\xe3\x83\xbc\xe3\x83\x89\xe3\x82\xaf\xe3\x83\xa9\xe3\x82\xb9,\xe6\x8e\xa5\xe7\xb6\x9a,\xe5\xb9\x85\xef\xbc\x88mm\xef\xbc\x89,\xe5\xa5\xa5\xe8\xa1\x8c\xe3\x81\x8d\xef\xbc\x88mm\xef\xbc\x89,\xe9\xab\x98\xe3\x81\x95\xef\xbc\x88mm\xef\xbc\x89,\xe9\x87\x8d\xe9\x87\x8f\xef\xbc\x88g\xef\xbc\x89,\xe4\xbf\x9d\xe8\xa8\xbc\xef\xbc\x88\xe5\xb9\xb4\xef\xbc\x89,\xe3\x82\xb1\xe3\x83\xbc\xe3\x83\x96\xe3\x83\xab,\xe5\x93\x81\xe5\x90\x8d\r\n
type,maker,prefix,series,storage,model,price,date,url,speed_read,speed_write,speed_class,usb,width,depth,height,weight,warranty,cable,name\r\n
microSDXC,SanDisk,SD,QXA1,128,SDSQXA1-128G,2680,2020-05-07,https://www.amazon.co.jp/dp/B07H89SQTH,160,90,U3/V30/A2,,,,,,,,\r\n
microSDXC,SanDisk,SD,QXA1,256,SDSQXA1-256G,5600,2020-05-07,https://www.amazon.co.jp/dp/B07H86LWMS,160,90,U3/V30/A2,,,,,,,,\r\n
microSDXC,SanDisk,SD,QXCZ,512,SDSQXCZ-512G,12398,2020-05-07,https://www.amazon.co.jp/dp/B07RKL4L7Q,,,U3/V30/A2,,,,,,,,\r\n
SDXC,SanDisk,SD,DXV5,128,SDSDXV5-128G-GHENN,4183,2020-05-07,https://www.amazon.co.jp/dp/B07XP3GPC3,150,70,C10/U3/V30,,,,,,,,\r\n
SDXC,SanDisk,SD,DXV5,256,SDSDXV5-256G-GHENN,7123,2020-05-07,https://www.amazon.co.jp/dp/B07XNYCQJT,150,70,C10/U3/V30,,,,,,,,\r\n
SDXC,SanDisk,SD,DXXY,128,SDSDXXY-128G-GN4IN,3910,2020-05-07,https://www.amazon.co.jp/dp/B07H9DVLBB,170,,C10/U3/V30,,,,,,,,\r\n
SDXC,SanDisk,SD,DXXY,256,SDSDXXY-256G-GN4IN,7350,2020-05-07,https://www.amazon.co.jp/dp/B07H9VX76D,170,,C10/U3/V30,,,,,,,,\r\n
'

```{code-cell} ipython3
with urllib.request.urlopen(export_url) as u:
    print(type(u))
    print(u.read())
```

リンクが公開されていない場合 -> HTMLが返ってくる

```
<!doctype html><html lang="en" dir="ltr"><head><base href="https://accounts.google.com/v3/signin/">...
```

```{code-cell} ipython3
with urllib.request.urlopen(export_url) as u:
    print(u.read())
```

```{code-cell} ipython3

```
