---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: .venv
  language: python
  name: python3
---

# httpxの使い方を確認

- https://www.python-httpx.org/
- https://httpbin.org にアクセスして動作を確認する
- `requests`と同じように使用できるかを確認する

```{code-cell} ipython3
import httpx

print(f"{httpx.__version__=}")

response = httpx.get("https://httpbin.org/get")
response.json()
```

```{code-cell} ipython3
response = httpx.get("https://httpbin.org/status/200", timeout=30)
response
```

```{code-cell} ipython3
response = httpx.get("https://httpbin.org/status/300", timeout=30)
response
```

```{code-cell} ipython3
response = httpx.get("https://httpbin.org/status/400", timeout=30)
response
```

```{code-cell} ipython3
response.reason_phrase
```

ステータスコードと`response.is_success`を確認する

- `httpx`の場合は`response.ok`ではなく`response.is_success`を使う
- 200番台、300番台: `response.ok = True`
- 400番台、500番台: `response.ok = False`

```{code-cell} ipython3
try:
    response = httpx.get("https://httpbin.org/status/200", timeout=30)
    print(
        f"{response.is_success=} / {response.status_code=}: {response.reason_phrase=}"
    )

    response = httpx.get("https://httpbin.org/status/300", timeout=30)
    print(
        f"{response.is_success=} / {response.status_code=}: {response.reason_phrase=}"
    )

    response = httpx.get("https://httpbin.org/status/400", timeout=30)
    print(
        f"{response.is_success=} / {response.status_code=}: {response.reason_phrase=}"
    )

    response = httpx.get("https://httpbin.org/status/500", timeout=30)
    print(
        f"{response.is_success=} / {response.status_code=}: {response.reason_phrase=}"
    )

    response.raise_for_status()
except Exception as e:
    print(e)
```

# Google Spreadsheetが共有されているか確認

- 共有／非共有のシートを用意した
  - 共有 -> 「リンクを知っている全員」
  - 非共有 -> 非公開

```{code-cell} ipython3
# 20200406_snapsheet_sample_shared
SHARED_URL = "https://docs.google.com/spreadsheets/d/1NbSH0rSCLkElG4UcNVuIhmg5EfjAk3t8TxiBERf6kBM/edit#gid=0"

# 20200406_snapsheet_sample_unshared
UNSHARED_URL = "https://docs.google.com/spreadsheets/d/1g1yVx-r1d0V1YixUygvO3ASS7HzZFmSj9NlyDT1-lUw/edit#gid=0"
```

## ``httpx.get``したレスポンスを確認する

```{code-cell} ipython3
import httpx

print("shared")
try:
    shared = httpx.get(SHARED_URL, timeout=10)
    shared.raise_for_status()
except Exception as e:
    print(f"{e=}")
finally:
    print(f"{shared.is_success=}: {shared.status_code}: {shared.reason_phrase=}")

print("unshared")
try:
    unshared = httpx.get(UNSHARED_URL, timeout=10)
    unshared.raise_for_status()
except Exception as e:
    print(f"{e=}")
finally:
    print(f"{unshared.is_success=}: {unshared.status_code}: {unshared.reason_phrase=}")
```

- `requests`と同じようにレスポンスコードで判別できる
- ``unshared``はURLのレスポンスが`401`（Unauthorized）になる
- `raise_for_status()`で例外を送ればOK

## 非同期アクセス

- `httpx`は非同期（`asyncio`）アクセスにも対応している
