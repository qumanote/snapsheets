---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3.9.13 ('snapsheets-mU0WpA-6-py3.9')
  language: python
  name: python3
---

TOMLとYAMLファイルの読み込み方を確認する

```{code-cell} ipython3
from pathlib import Path

fname1 = Path("../sandbox/config.toml")
```

```{code-cell} ipython3
import tomli
```

TOMLの設定ファイルを読み込む

```{code-cell} ipython3
with fname1.open("rb") as f:
    config = tomli.load(f)
```

```{code-cell} ipython3
config
```

この辞書型 ``config``をYAMLに変換する

```{code-cell} ipython3
import yaml
```

```{code-cell} ipython3
print(yaml.dump(config))
```

これを``../sandbox/config.yaml``にコピペする

```{code-cell} ipython3
fname2 = Path("../sandbox/config.yaml")
```

```{code-cell} ipython3
with fname2.open("r") as f:
    config_yaml = yaml.safe_load(f)
```

```{code-cell} ipython3
config_yaml
```

```{code-cell} ipython3
config == config_yaml
```

```{code-cell} ipython3

```
