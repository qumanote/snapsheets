---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3.10.4 ('snapsheets-mU0WpA-6-py3.10')
  language: python
  name: python3
---

``Loguru``の使い方を確認する

- GitHub : https://github.com/Delgan/loguru
- Document : https://loguru.readthedocs.io/

```{code-cell} ipython3
import sy
from loguru import logger
```

ログ出力はデフォルトで``stderr``に出力される

- ``logging``モジュールはいろいろ設定が必要だが、これは特になにも設定しなくていいので楽ちん

```{code-cell} ipython3
logger.debug("That's it, beautiful and simple logging!")
```

- 利用できるレベルを確認した
- 表示される内容は ``{time} | {level} | {name}:{function}:{line} - {message}`` になっている

```{code-cell} ipython3
logger.trace("TRACE = 5. Not printed.")
logger.debug("DEBUG = 10")
logger.info("INFO = 20")
logger.success("SUCCESS = 25")
logger.warning("WARNING = 30")
logger.error("ERROR = 40")
logger.critical("CRITICAL = 50")
```

## ハンドラーを設定

- ``logger.add``でハンドラーを追加できる
  - 返り値は登録されているハンドラーの数（っぽい）
- ``INFO``レベル（以上）の表示を変更
- ``logger.remove``でハンドラーを削除できる
- 利用できるレコード名（＝テンプレート変数のようなもの）はドキュメントを参照
  - https://loguru.readthedocs.io/en/stable/api/logger.html#record
  - ``{function}`` は ``{module}.py`` になる

```{code-cell} ipython3
def configure_logger(debug: bool) -> None:
    """Configure loguru logger"""

    # ハンドラーを初期化（空にする）
    logger.remove()
    if debug:
        fmt = (" | ").join(
            [
                "{time:YYYY_MM_DDTHH:mm:ss}",
                "<level>{level:8}</level>",
                "<cyan>{name}.{function}:{line}</cyan>",
                "<level>{message}</level>",
            ]
        )
        logger.add(sys.stderr, format=fmt, level="DEBUG")
    else:
        fmt = (" | ").join(
            [
                "{time:YYYY_MM_DDTHH:mm:ss}",
                "<level>{level:8}</level>",
                "<level>{message}</level>",
            ]
        )
        logger.add(sys.stderr, format=fmt, level="SUCCESS")
```

```{code-cell} ipython3
# debug = True
configure_logger(debug=True)

logger.trace("TRACE = 5. Not printed.")
logger.debug("DEBUG = 10. Printed.")
logger.info("INFO = 20. Printed.")
logger.success("SUCCESS = 25. Printed.")
logger.warning("WARNING = 30. Printed.")
logger.error("ERROR = 40. Printed.")
logger.critical("CRITICAL = 50. Printed.")
```

```{code-cell} ipython3
# debug = False -> SUCCESS以上を表示
configure_logger(debug=False)

logger.trace("TRACE = 5. Not printed.")
logger.debug("DEBUG = 10. Not printed.")
logger.info("INFO = 20. Not Printed.")
logger.success("SUCCESS = 25. Printed")
logger.warning("WARNING = 30. Printed.")
logger.error("ERROR = 40. Printed.")
logger.critical("CRITICAL = 50. Printed.")
```

```{code-cell} ipython3

```
