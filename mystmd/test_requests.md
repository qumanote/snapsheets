---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: .venv
  language: python
  name: python3
---

# requestsの使い方を確認

- https://docs.python-requests.org/en/latest/index.html
- https://httpbin.org にアクセスして動作を確認する

```{code-cell} ipython3
import requests

print(f"{requests.__version__}")

response = requests.get("https://httpbin.org/get")
response.json()
```

```{code-cell} ipython3
response = requests.get("https://httpbin.org/status/200", timeout=30)
response
```

```{code-cell} ipython3
response = requests.get("https://httpbin.org/status/300", timeout=30)
response
```

```{code-cell} ipython3
response = requests.get("https://httpbin.org/status/400", timeout=30)
response
```

ステータスコードと`response.ok`を確認する

- 200番台、300番台: `response.ok = True`
- 400番台、500番台: `response.ok = False`
  - `response.raise_for_status()`で例外を発生させることができる

```{code-cell} ipython3
try:
    response = requests.get("https://httpbin.org/status/200", timeout=30)
    print(f"{response.ok=} / {response.status_code=}: {response.reason=}")

    response = requests.get("https://httpbin.org/status/300", timeout=30)
    print(f"{response.ok=} / {response.status_code=}: {response.reason=}")

    response = requests.get("https://httpbin.org/status/400", timeout=30)
    print(f"{response.ok=} / {response.status_code=}: {response.reason=}")

    response = requests.get("https://httpbin.org/status/500", timeout=30)
    print(f"{response.ok=} / {response.status_code=}: {response.reason=}")

    response.raise_for_status()
except Exception as e:
    print(e)
```

# Google Spreadsheetが共有されているか確認

- 共有／非共有のシートを用意した
  - 共有 -> 「リンクを知っている全員」
  - 非共有 -> 非公開

```{code-cell} ipython3
# 20200406_snapsheet_sample_shared
SHARED_URL = "https://docs.google.com/spreadsheets/d/1NbSH0rSCLkElG4UcNVuIhmg5EfjAk3t8TxiBERf6kBM/edit#gid=0"

# 20200406_snapsheet_sample_unshared
UNSHARED_URL = "https://docs.google.com/spreadsheets/d/1g1yVx-r1d0V1YixUygvO3ASS7HzZFmSj9NlyDT1-lUw/edit#gid=0"
```

## ``requests.get``したレスポンスを確認する

```{code-cell} ipython3
import requests

print("shared")
try:
    shared = requests.get(SHARED_URL, timeout=10)
    shared.raise_for_status()
except Exception as e:
    print(f"{e=}")
finally:
    print(f"{shared.ok=}: {shared.status_code}: {shared.reason=}")

print("unshared")
try:
    unshared = requests.get(UNSHARED_URL, timeout=10)
    unshared.raise_for_status()
except Exception as e:
    print(f"{e=}")
finally:
    print(f"{unshared.ok=}: {unshared.status_code}: {unshared.reason=}")
```

- レスポンスコードで判別できる
- ``unshared``はURLのレスポンスが`401`（Unauthorized）になる
- `raise_for_status()`で例外を送ればOK

```{code-cell} ipython3
# リンクの確認
print(f"{shared.links=}")
print(f"{unshared.links=}")
```

```{code-cell} ipython3
# 取得したテキストを確認 -> 長いので文字数
print(f"{len(shared.text)=}")
print(f"{len(unshared.text)=}")
```
