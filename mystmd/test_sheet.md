---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3.9.13 ('snapsheets-mU0WpA-6-py3.9')
  language: python
  name: python3
---

# ``Sheet``クラスの設計

```python
sheet = Sheet(...)
sheet.url = "スプレッドシートのURLを設定する"
sheet.filename = "保存するファイル名を設定する"

sheet.download()
sheet.backup()
sheet.snapshot()
```

+++

## テスト用URL

- Googleスプレッドシート -> 共有 -> 「リンクを知っている全員」

```{code-cell} ipython3
SHARED_URL1 = "https://docs.google.com/spreadsheets/d/16Sc_UgShNuxMfRnBiFsjmfThE1VfVhJf3jgmxNvFeEI/edit?gid=130812342#gid=130812342"
SHARED_URL2 = "https://docs.google.com/spreadsheets/d/16Sc_UgShNuxMfRnBiFsjmfThE1VfVhJf3jgmxNvFeEI/edit?gid=0#gid=0"
SHARED_URL3 = "https://docs.google.com/spreadsheets/d/16Sc_UgShNuxMfRnBiFsjmfThE1VfVhJf3jgmxNvFeEI/edit?gid=867348879#gid=867348879"
SHARED_URL4 = "https://docs.google.com/spreadsheets/d/16Sc_UgShNuxMfRnBiFsjmfThE1VfVhJf3jgmxNvFeEI/edit?gid=2015536778#gid=2015536778"
```

## URL処理

- URL文字列から``KEY``と``GID``を取得する
- `urllib.parse.urlparse`でURL処理する

```{code-cell} ipython3
from urllib.parse import urlparse

parsed = urlparse(SHARED_URL1)
print(f"{parsed.scheme=}")
print(f"{parsed.netloc=}")
print(f"{parsed.path=}")
print(f"{parsed.params=}")
print(f"{parsed.query=}")
print(f"{parsed.fragment=}")
```

## ドメインを確認する

- ドメイン（`netloc`）が`docs.google.com`でない場合は終了

```{code-cell} ipython3
parsed.netloc in ["docs.google.com"]
```

## `KEY`を抽出する

- `parsed.path`を処理する
- "`/`"でスプリットし、後ろから2つ目が`KEY`

```{code-cell} ipython3
parsed.path.split("/")[-2]
```

## `GID`を抽出する

- `parsed.fragment`を処理する
- フラグメントがない場合は`0`（GID=0）にする

```{code-cell} ipython3
parsed.fragment.split("=")[1]
```

## エクスポート用のURLを生成する

- `https://docs.google.com/spreadsheets/d/{KEY}/export?gid={GID}&format={FORMAT}`
- `https://docs.google.com/spreadsheets/d/{KEY}/export?format={FORMAT}&gid={GID}`

```{code-cell} ipython3
from urllib.parse import urlencode

key = "16Sc_UgShNuxMfRnBiFsjmfThE1VfVhJf3jgmxNvFeEI"
gid = "0"
fmt = "csv"


q = {"gid": gid, "format": fmt}
# クエリの順番は入れ替えてもOK
# q = {"format": fmt, "gid": gid}
query = urlencode(q)
base_url = f"https://docs.google.com/spreadsheets/d/{key}/export"

export_url = ("?").join((base_url, query))

print(f"{key=}")
print(f"{gid=}")
print(f"{query=}")
print(f"{base_url=}")
print(f"{export_url=}")
```

## あとで整理する

```{code-cell} ipython3
from urllib.parse import urlparse
from dataclasses import dataclass
from pathlib import Path
from icecream import ic
import pendulum
import subprocess
import shutil


@dataclass
class Sheet:
    url: str
    filename: str
    description: str
    datefmt: str
    skip: bool

    def __post_init__(self):
        p = urlparse(self.url)
        if p.netloc not in ["docs.google.com"]:
            error = f"URL should start with 'https://docs.google.com/' : {self.url}"
            ic(error)
            sys.exit()

        p = Path(self.filename)
        self.suffix = p.suffix
        self.fmt = self.get_fmt()

        self.key = self.get_key()
        self.gid = self.get_gid()
        self.export_url = self.get_export_url()

    def get_fmt(self):
        ok = ["xlsx", "ods", "csv", "tsv"]
        fmt = Path(self.filename).suffix.strip(".")
        if fmt not in ok:
            error = f"{fmt} is a wrong format. Select from {ok}."
            ic(error)
            sys.exit()
        return fmt

    def get_key(self):
        """与えられたURLからkeyを取得する"""
        p = urlparse(self.url)
        key = p.path.split("/")[3]
        return key

    def get_gid(self):
        """与えられたURLからgidを取得する

        gidが見つからない場合は 0 にする
        """
        p = urlparse(self.url)
        if not p.fragment:
            return 0
        gid = p.fragment.split("=")[1]
        return gid

    def get_export_url(self):
        path = f"https://docs.google.com/spreadsheets/d/{self.key}/export"
        query = f"format={self.fmt}"
        if self.gid:
            query += f"&gid={self.gid}"
        url = f"{path}?{query}"
        return url

    def download(self):
        """download spreadsheet"""
        p = Path(self.filename)
        if not p.parent.exists():
            error = f"Unable to find the destination. Perhaps you need to create a directory named '{p.parent}'."
            ic(error)
            sys.exit()

        cmd = ["wget", "--quiet", "-O", self.filename, self.url]
        cmd = [str(c) for c in cmd if c]
        if self.skip:
            warning = f"Skipped downloading {self.filename}."
            ic(warning)
        else:
            subprocess.run(cmd)
            info = f"🤖 Downloaded as {self.filename}"
            ic(info)
        pass

    def backup(self):
        p = Path(self.filename)
        if not p.exists():
            error = f"Unable to find source file. {p}"
            ic(error)
            sys.exit()

        now = pendulum.now().strftime(self.datefmt)
        fname = f"{now}_{p.name}"
        movef = Path(p.parent, fname)
        if self.skip:
            warning = f"Skipped renaming {self.filename}"
            ic(warning)
        else:
            shutil.move(self.filename, movef)
            info = f"🚀 Renamed to {movef}"
            ic(info)
        pass

    def snapshot(self):
        """backup & download"""
        info = f"📣 {self.description}"
        ic(info)
        self.download()
        self.backup()

    def info(self):
        ic("----- Expricit members -----")
        ic(self.url)
        ic(self.filename)
        ic(self.description)
        ic(self.datefmt)
        ic(self.skip)
        ic("----- Implicit members -----")
        ic(self.suffix)
        ic(self.fmt)
        ic(self.key)
        ic(self.gid)
        ic(self.export_url)
```

とりあえずデフォルトの引数は設定せず、すべての値を手動で入力する

```{code-cell} ipython3
url = "https://docs.google.com/spreadsheets/d/16Sc_UgShNuxMfRnBiFsjmfThE1VfVhJf3jgmxNvFeEI/edit#gid=2015536778"
filename = "test_data/sample1.csv"
description = "Testing re-defined Sheet class"
skip = False
datefmt = "%Y%m%d"

sheet = Sheet(
    url=url, filename=filename, description=description, datefmt=datefmt, skip=skip
)
sheet.info()
```

ファイル名の拡張子を間違えるとエラーを表示して終了させる

```{code-cell} ipython3
filename = "test_data/sample1.pdf"
sheet = Sheet(
    url=url, filename=filename, description=description, datefmt=datefmt, skip=skip
)
```

ダウンロードできるか確認

- 保存先のディレクトリが存在しない場合は、エラーを表示して終了する
- ディレクトリを作成することを促すメッセージを表示する

```{code-cell} ipython3
filename = "test_data/more/sample1.csv"
sheet = Sheet(
    url=url, filename=filename, description=description, datefmt=datefmt, skip=skip
)
sheet.download()
sheet.backup()
```

```{code-cell} ipython3
filename = "test_data/sample1.csv"
sheet = Sheet(
    url=url, filename=filename, description=description, datefmt=datefmt, skip=skip
)
sheet.snapshot()
```

``snapsheet.next``モジュールから読み込む

```{code-cell} ipython3
from snapsheets.next import Sheet
```

```{code-cell} ipython3
url = "https://docs.google.com/spreadsheets/d/16Sc_UgShNuxMfRnBiFsjmfThE1VfVhJf3jgmxNvFeEI/edit#gid=2015536778"
filename = "test_data/sample1.csv"
description = "Test re-defined snapsheets.next.Sheet class"

sheet = Sheet(url=url, filename=filename, description=description)
sheet.snapshot()
```

```{code-cell} ipython3

```
