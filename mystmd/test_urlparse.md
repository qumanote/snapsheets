---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: .venv
  language: python
  name: python3
---

# URL操作を確認

- `urllib.parse`モジュールを使ってURLをいじる
- GoogleシートのURLからexport用のURLに変換したい

## GoogleシートのURL

- `https://docs.google.com/spreadsheets/d/KEY/edit?gid=GID#gid=GID`
- 最初に作るシートは`gid=0`
- クエリを省略した場合も`gid=0`にリダイレクトされる
- `gid=0`のシートがない場合も既存のシートにリダイレクトされる

## export用のURL

- `https://docs.google.com/spreadsheets/d/KEY/export?gid=GID&format=FORMAT`
- `edit`を`export`に置き換える
- `{"gid": "GID", "format": "FORMAT"}`の形のクエリを追加する

```{code-cell} ipython3
from urllib import parse
```

`urllib.parse`はURLにアクセスすることないが、サンプルは`example.com`にする

```{code-cell} ipython3
SHEET0 = "https://docs.google.example.com/spreadsheets/d/KEY/edit?gid=0#gid=0"
SHEET1 = "https://docs.google.example.com/spreadsheets/d/KEY/edit?gid=GID#gid=GID"
```

```{code-cell} ipython3
p0 = parse.urlparse(SHEET0)
print(f"{p0.scheme=}")
print(f"{p0.netloc=}")
print(f"{p0.path=}")
print(f"{p0.params=}")
print(f"{p0.query=}")
print(f"{p0.fragment=}")
```

- `urlsplit`も同じようなことができる
- `.params`がない

```{code-cell} ipython3
s0 = parse.urlsplit(SHEET0)
print(f"{s0.scheme=}")
print(f"{s0.netloc=}")
print(f"{s0.path=}")
# params はない
print(f"{s0.query=}")
print(f"{s0.fragment=}")
s0
```

## export用URLのベースを作成する

- URLを置換する
- `parsed_url.path`の`/edit`を`/export`に置換する

```{code-cell} ipython3
print(f"{p0.scheme=}")
print(f"{p0.path=}")
replaced_url = p0.path.replace("/edit", "/export")
print(f"{replaced_url=}")

parse.urlunparse((p0.scheme, p0.netloc, replaced_url, "", "", ""))
```

## クエリを作成する

- `parsed_url.query`に`{"format": "FMT"}`を追加する
- `parse.urlencode`で文字列に変換する（`doseq=True`が必要）

```{code-cell} ipython3
q = parse.parse_qs(p0.query)
print(f"{q=}")
q["format"] = "FMT1"  # リストでなくてもよい
# q["format"] = ["FMT1", "FMT2"]
print(f"{q=}")
query = parse.urlencode(q, doseq=True)
```

## URLを整理する

- `requests`と`httpx`の場合、これまでに作成した`export_url`と`query`をパラメータに渡せばOK
- `wget`する場合、`parse.urlencode`でクエリを文字列に変換し、
- パース（`urlparse`）したURLは再連結できる（`urlunparse`）
- `urlsplit`と`urlunsplit`も同じ関係

```{code-cell} ipython3
s0 = parse.urlsplit(SHEET0)
print(f"{s0.scheme=}")
print(f"{s0.netloc=}")
print(f"{s0.path=}")
# params はない
print(f"{s0.query=}")
print(f"{s0.fragment=}")
s0
```

## wgetする

- `wget`はURLにフルパス

```{code-cell} ipython3

```
