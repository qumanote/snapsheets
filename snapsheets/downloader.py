import subprocess
import httpx
import requests
from loguru import logger
from pathlib import Path
from typing import Any


def via_requests(url: str, params: dict, filename: str) -> dict[str, Any]:
    """Download spreadsheet via requests.get

    - Download using `requests` module
    - Output filename can be configured with CLI option and config.

    Parameters
    ----------
    url: str
        base URL
    params: dict
        query
    filename: str
        output filename

    Returns
    -------
    dict[str, any]
    """
    msg = f"Download via requests.get({url})"
    logger.debug(msg)

    try:
        response = requests.get(
            url=url, params=params, timeout=30, allow_redirects=True
        )
        response.raise_for_status()
        logger.debug(f"{response.url=}")
        if response.ok:
            p = Path(filename)
            p.write_text(response.text, encoding="utf-8")
            msg = f"🤖 Downloaded as {filename}"
            logger.success(msg)
        return {"ok": response.ok, "err": None}
    except Exception as e:
        msg = f"Failed to download the file: {e}"
        logger.error(msg)
        return {"ok": False, "err": e}


def via_httpx(url: str, params: dict, filename: str) -> dict[str, Any]:
    """Download spreadsheet via httpx.get

    - Downlaod using `httpx` module
    - Output filename can be configured with CLI option and config.

    Parameters
    ----------
    filename: str
        output filename
    export_url: str
        download URL

    Returns
    --------
    dict[str, any]
    """
    msg = f"Download via httpx.get({url})"
    logger.debug(msg)

    try:
        response = httpx.get(url=url, params=params, follow_redirects=True)
        response.raise_for_status()
        logger.debug(f"{response.url=}")
        if response.is_success:
            p = Path(filename)
            p.write_text(response.text, encoding="utf-8")
            msg = f"🤖 Downloaded as {filename}"
            logger.success(msg)
        return {"ok": response.is_success, "err": None}
    except Exception as e:
        msg = f"Failed to download the file: {e}"
        logger.error(msg)
        return {"ok": False, "err": e}


def via_wget(url: str, params: str, filename: str) -> dict[str, Any]:
    """Download spreadsheet via wget

    - Download using `wget` command
    - Output filename can be configured with CLI option and config.

    Parameters
    ----------
    filename : str
        output filename
    export_url : str
        download URL

    Returns
    --------
    dict[str, any]
    """
    msg = "Download via wget"
    logger.debug(msg)

    cmd = ["wget", "--quiet", "-O", filename, url]
    cmd = [str(c) for c in cmd if c]
    try:
        # check=True to raise an exception automatically.
        subprocess.run(cmd, check=True)
        msg = f"🤖 Downloaded as {filename}"
        logger.success(msg)
        return {"ok": True, "err": None}
    except subprocess.CalledProcessError as e:
        msg = f"Failed to download the file: {e}"
        logger.error(msg)
        return {"ok": False, "err": e}
