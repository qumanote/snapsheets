import pytest
from unittest.mock import patch, mock_open
from pathlib import Path
from snapsheets.book import Book
from snapsheets.sheet import Sheet

TEST_CONFIG_TOML = """
[[sheets]]
url = "https://docs.google.com/spreadsheets/d/16Sc_UgShNuxMfRnBiFsjmfThE1VfVhJf3jgmxNvFeEI/edit#gid=2015536778"
filename = "snapd/sample1.csv"
desc = "Sample spreadsheet for snapsheets. (%Y%m%d)"
datefmt = "%Y%m%d"
skip = false

[[sheets]]
url = "https://docs.google.com/spreadsheets/d/16Sc_UgShNuxMfRnBiFsjmfThE1VfVhJf3jgmxNvFeEI/edit#gid=0"
filename = "snapd/sample2.csv"
desc = "Sample spreadsheet for snapsheets. (%Y%m)"
datefmt = "%Y%m"
skip = false

[[sheets]]
url = "https://docs.google.com/spreadsheets/d/16Sc_UgShNuxMfRnBiFsjmfThE1VfVhJf3jgmxNvFeEI/edit#gid=0"
filename = "snapd/sample3.csv"
desc = "Sample spreadsheet for snapsheets. (%Y%m)"
datefmt = "%Y%m"
skip = true
"""

TEST_CONFIG_YAML = """
sheets:
- datefmt: '%Y%m%d'
  desc: Sample spreadsheet for snapsheets. (%Y%m%d)
  filename: snapd/sample1.csv
  skip: false
  url: https://docs.google.com/spreadsheets/d/16Sc_UgShNuxMfRnBiFsjmfThE1VfVhJf3jgmxNvFeEI/edit#gid=2015536778
- datefmt: '%Y%m'
  desc: Sample spreadsheet for snapsheets. (%Y%m)
  filename: snapd/sample2.csv
  skip: false
  url: https://docs.google.com/spreadsheets/d/16Sc_UgShNuxMfRnBiFsjmfThE1VfVhJf3jgmxNvFeEI/edit#gid=0
- datefmt: '%Y%m'
  desc: Sample spreadsheet for snapsheets. (%Y%m)
  filename: snapd/sample3.csv
  skip: true
  url: https://docs.google.com/spreadsheets/d/16Sc_UgShNuxMfRnBiFsjmfThE1VfVhJf3jgmxNvFeEI/edit#gid=0
"""


@patch("pathlib.Path.open", new_callable=mock_open, read_data=TEST_CONFIG_TOML)
@patch("pathlib.Path.exists", return_value=True)
def test_load_config_from_toml(mock_exists, mock_open):
    """Test loading configuration from TOML"""
    # Create a Book instance with a mock config file path
    book = Book(fname="mock_config.toml")

    # Check if the sheets list is correctly populated
    assert len(book.sheets) == 0

    # Check details of the first sheet
    # sheet = book.sheets[0]
    # assert isinstance(sheet, Sheet)
    # url = "https://docs.google.com/spreadsheets/d/16Sc_UgShNuxMfRnBiFsjmfThE1VfVhJf3jgmxNvFeEI/edit#gid=2015536778"
    # assert sheet.url == url


@patch("pathlib.Path.open", new_callable=mock_open, read_data=TEST_CONFIG_YAML)
@patch("pathlib.Path.exists", return_value=True)
def test_load_config_from_yaml(mock_exists, mock_open):
    """Test loading configuration from YAML"""
    # Create a Book instance with a mock config file path
    book = Book(fname="mock_config.yaml")

    # Check if the sheets list is correctly populated
    assert len(book.sheets) == 0

    # Check details of the first sheet
    # sheet = book.sheets[0]
    # assert isinstance(sheet, Sheet)
    # url = "https://docs.google.com/spreadsheets/d/16Sc_UgShNuxMfRnBiFsjmfThE1VfVhJf3jgmxNvFeEI/edit#gid=2015536778"
    # assert sheet.url == url
