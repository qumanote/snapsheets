import pytest
from unittest.mock import patch, mock_open
from snapsheets.downloader import via_httpx, via_wget, via_requests


@patch("pathlib.Path.write_text")
@patch("requests.get")
def test_via_requests(mock_get, mock_write):
    """Test download method via httpx

    Mocks requests.get to test the download method
    without actually making a network call.

    Mocks pathlib.Path.write_text to
    test without actually writing to a file.
    """

    # test strings
    fname = "output.csv"
    url = "https://example.com"
    params = None
    text = "sample csv content"

    # mocks return values of requests.get
    mock_get.return_value.ok = True
    mock_get.return_value.text = text

    # test call via_requests
    via_requests(url=url, params=params, filename=fname)

    # checks if httpx.get was called with proper args
    mock_get.assert_called_once_with(
        url=url, params=params, timeout=30, allow_redirects=True
    )

    # mocks pathlib.Path.write_text
    # check if text was written correctly with utf-8 encoding
    mock_write.assert_called_once_with(text, encoding="utf-8")


@patch("pathlib.Path.write_text")
@patch("httpx.get")
def test_via_httpx(mock_get, mock_write):
    """Test download method via httpx

    Mocks httpx.get to test the download method
    without actually making a network call.

    Mocks pathlib.Path.write_text to
    test without actually writing to a file.
    """

    # test strings
    fname = "output.csv"
    url = "https://example.com"
    params = None
    text = "sample csv content"

    # mocks return values of httpx.get
    mock_get.return_value.is_success = True
    mock_get.return_value.text = text

    # test call via_httpx
    via_httpx(url=url, params=params, filename=fname)

    # checks if httpx.get was called with proper args
    mock_get.assert_called_once_with(url=url, params=params, follow_redirects=True)

    # mocks pathlib.Path.write_text
    # check if text was written correctly with utf-8 encoding
    mock_write.assert_called_once_with(text, encoding="utf-8")


@patch("subprocess.run")
def test_via_wget(mock_subprocess_run):
    """Test download method via wget

    Mocks subprocess.run to test the download method
    without actually making a network call or downloading any files
    """
    fname = "output.csv"
    url = "https://example.com"

    via_wget(url=url, params=None, filename=fname)

    cmd = ["wget", "--quiet", "-O", fname, url]
    cmd = [str(c) for c in cmd if c]
    mock_subprocess_run.assert_called_with(cmd, check=True)
