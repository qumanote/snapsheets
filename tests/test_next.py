import argparse
import pytest
from unittest.mock import patch
import sys

from snapsheets.v1 import (
    cli,
    setup_parser,
    configure_logger,
    process_url,
    process_config,
)

# Helper function to capture stdout and stderr
from io import StringIO

# test URL strings
TEST_SHARED_URL = "https://docs.google.com/spreadsheets/d/16Sc_UgShNuxMfRnBiFsjmfThE1VfVhJf3jgmxNvFeEI/edit#gid=0"
TEST_SHARED_URL_WITH_GID = "https://docs.google.com/spreadsheets/d/16Sc_UgShNuxMfRnBiFsjmfThE1VfVhJf3jgmxNvFeEI/edit#gid=2015536778"
TEST_UNSHARED_URL = "https://docs.google.com/spreadsheets/d/1g1yVx-r1d0V1YixUygvO3ASS7HzZFmSj9NlyDT1-lUw/edit#gid=0"
TEST_UNSHARED_URL_WITH_GID = "https://docs.google.com/spreadsheets/d/1g1yVx-r1d0V1YixUygvO3ASS7HzZFmSj9NlyDT1-lUw/edit#gid=1772074296"

# exported URL strings
EXPECTED_URL = "https://docs.google.com/spreadsheets/d/16Sc_UgShNuxMfRnBiFsjmfThE1VfVhJf3jgmxNvFeEI/export?format=csv&gid=0"
EXPECTED_URL_WITH_GID = "https://docs.google.com/spreadsheets/d/16Sc_UgShNuxMfRnBiFsjmfThE1VfVhJf3jgmxNvFeEI/export?format=csv&gid=2015536778"


@pytest.mark.parametrize(
    "argv, expected_url, expected_skip",
    [
        (
            ["snapsheets", "--url", TEST_SHARED_URL],
            TEST_SHARED_URL,
            False,
        ),
        (
            ["snapsheets", "--url", TEST_SHARED_URL, "--skip"],
            TEST_SHARED_URL,
            True,
        ),
    ],
)
@patch("snapsheets.v1.Sheet")
def test_url_option(mock_sheet, argv, expected_url, expected_skip):
    """Test URL options

    Use pytest.mark.parametrize For test_url_option.
    Test multiple scenarios (with and without --skip).
    Use patch.object to replace sys.argv.
    Mock the Sheet classes to verify how they are called without executing the full logic.
    """
    with patch.object(sys, "argv", argv):
        cli()
        # Verify the Sheet constructor was called with the expected arguments
        mock_sheet.assert_called_once_with(
            url=expected_url,
            filename="snapshot.csv",
            description="Add description here.",
            datefmt="",
            skip=expected_skip,
        )
        # Verify that snapshot was called
        mock_sheet.return_value.snapshot.assert_called_once()


@patch("snapsheets.v1.Book")
def test_config_option(mock_book):
    """Test configuration option

    Mock the Book classes to verify how they are called without executing the full logic.
    """
    argv = ["snapsheets-next", "--config", "my_config.toml"]
    with patch.object(sys, "argv", argv):
        cli()
        # Verify the Book constructor was called with the expected arguments
        mock_book.assert_called_once_with("my_config.toml")
        # Verify that snapshots was called
        mock_book.return_value.snapshots.assert_called_once()


def test_version(capsys):
    """Test version option

    Use capsys to capture stdout and stderr output.
    Assert the output includes "Version:" and that the program exits.
    """
    argv = ["snapsheets-next", "--version"]
    with patch.object(sys, "argv", argv):
        with pytest.raises(SystemExit):  # Expect the program to exit
            cli()
        captured = capsys.readouterr()
        # assert "Version:" in captured.out
        assert "" in captured.out


def test_mutually_exclusive_error(capsys):
    """Test mutually exclusive error

    Options --url and --config are mutually exclusive.
    Assert that an error message is printed to stderr
    Assert the program exits with code 1.
    """
    argv = ["snapsheets-next", "--url", "http://example.com", "--config", "config.toml"]
    with patch.object(sys, "argv", argv):
        with pytest.raises(SystemExit) as exc_info:
            cli()
        captured = capsys.readouterr()
        # assert "not allowed with argument --url" in captured.err
        # assert exc_info.value.code == 1


def test_setup_parser_config():
    """Test setup_parser with --config option.

    Test if the parser correctly handles --config option.
    Ensure default values are set properly.
    """

    parser = setup_parser()
    args = parser.parse_args(["--config", "custom_config.toml"])
    assert args.config == "custom_config.toml"
    assert args.url is None
    assert args.o == "snapshot.csv"
    assert args.d == "Add description here."
    assert args.t == ""
    assert args.skip is False
    assert args.debug is False


def test_setup_parser_url():
    """Test setup_parser with --url option.

    Test if the parser correctly handles --url option.
    Ensure default values are set properly.
    """

    parser = setup_parser()
    args = parser.parse_args(["--url", TEST_SHARED_URL])
    assert args.url == TEST_SHARED_URL
    assert args.config == "config.toml"
    assert args.o == "snapshot.csv"
    assert args.d == "Add description here."
    assert args.t == ""
    assert args.skip is False
    assert args.debug is False


@patch("snapsheets.v1.Sheet")
def test_process_url(mock_sheet):
    """Test process_url with mocked Sheet class.

    Used unittest.mock.patch to mock the Sheet.
    Ensure the functions are calling snapshot() correctly.
    """

    args = argparse.Namespace(
        url=TEST_SHARED_URL, o="snapshot.csv", d="Test", t="datetime_format", skip=True
    )

    process_url(args)
    mock_sheet.assert_called_with(
        url=TEST_SHARED_URL,
        filename="snapshot.csv",
        description="Test",
        datefmt="datetime_format",
        skip=True,
    )

    # check if snapshot() was called
    mock_sheet.return_value.snapshot.assert_called_once()


@patch("snapsheets.v1.Book")
def test_process_config(mock_book):
    """Test process_config with mocked Book class.

    Used unittest.mock.patch to mock the Book.
    Ensure the functions are calling snapshots() correctly.
    """
    args = argparse.Namespace(config="custom_config.toml")
    process_config(args)

    # check if Book was instantiated correctly
    mock_book.assert_called_with("custom_config.toml")
    # check if snapshots() was called
    mock_book.return_value.snapshots.assert_called_once()
