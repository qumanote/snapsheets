import pytest
from unittest.mock import patch
from snapsheets.sheet import Sheet

# Test URL: Google sheet - Storage Comparison

# test URL strings
TEST_SHARED_URL = "https://docs.google.com/spreadsheets/d/KEY/edit#gid=0"
EXPECTED_URL = "https://docs.google.com/spreadsheets/d/KEY/export?gid=0&format=csv"
TEST_UNSHARED_URL = "https://docs.google.com/spreadsheets/d/KEY/edit#gid=0"

# test URL with GID strings
TEST_SHARED_URL_WITH_GID = (
    "https://docs.google.com/spreadsheets/d/KEY/edit#gid=2015536778"
)
EXPECTED_URL_WITH_GID = (
    "https://docs.google.com/spreadsheets/d/KEY/export?gid=2015536778&format=csv"
)
TEST_UNSHARED_URL_WITH_GID = (
    "https://docs.google.com/spreadsheets/d/KEY/edit#gid=1772074296"
)


@patch("requests.get")
def test_get_fmt(mock_get):
    """Test get_fmt method

    Test if the get_fmt method returns the correct format.
    Exits when an invalid format is provided.

    Mock requests.get to reduce access to Google Sheet.
    """
    mock_get.return_value.ok = True

    # Test valid formats
    valid_formats = ["xlsx", "ods", "csv", "tsv"]
    url = TEST_SHARED_URL
    for fmt in valid_formats:
        sheet = Sheet(
            url=url,
            filename=f"output.{fmt}",
            description="Test",
        )
        assert sheet.get_fmt() == fmt

    # Test invalid format
    url = TEST_SHARED_URL
    with pytest.raises(ValueError):
        sheet = Sheet(
            url=url,
            filename="output.txt",
            description="Test",
        )
        sheet.get_fmt()


@patch("requests.get")
def test_get_key(mock_get):
    """Test get_key method

    Ensure correct parsing of key (spreadsheet ID) from the URL.

    Mock requests.get to reduce access to Google Sheet.
    """
    mock_get.return_value.ok = True
    url = TEST_SHARED_URL
    sheet = Sheet(url=url, filename="output.csv", description="Test")
    assert sheet.get_key() == "KEY"


@patch("requests.get")
def test_get_gid(mock_get):
    """Test get_gid method

    Ensure correct parsing of gid (sheet ID) from the URL.

    Mock requests.get to reduce access to Google Sheet.
    """
    mock_get.return_value.ok = True
    url = TEST_SHARED_URL
    sheet = Sheet(url=url, filename="output.csv", description="Test")
    assert sheet.get_gid() == "0"  # Default gid if not specified

    url = TEST_SHARED_URL_WITH_GID
    sheet_with_gid = Sheet(url=url, filename="output.csv", description="Test")
    assert sheet_with_gid.get_gid() == "2015536778"


@patch("requests.get")
def test_get_export_url(mock_get):
    """Test get_export_url method

    Test if the export URL is correctly formed based on the input parameters.

    Mock requests.get to reduce access to Google Sheet.
    """
    mock_get.return_value.ok = True
    url = TEST_SHARED_URL
    sheet = Sheet(url=url, filename="output.csv", description="Test")
    assert sheet.get_export_url() == EXPECTED_URL

    url = TEST_SHARED_URL_WITH_GID
    sheet_with_gid = Sheet(url=url, filename="output.csv", description="Test")
    assert sheet_with_gid.get_export_url() == EXPECTED_URL_WITH_GID


@patch("requests.get")
def test_unshared_google_sheets(mock_get):
    """Test if unshared Google Sheets URLs are handled

    Mock requests.get call to simulate an unshared Google Sheets URL scenario.
    If the URL is unshared, self.skip should be set to True.
    """
    # Mock requests.get to return an empty cookies list
    mock_get.return_value.ok = False
    url = TEST_UNSHARED_URL
    sheet = Sheet(url=url, filename="output.csv", description="Test")
    assert sheet.skip == True  # Should set skip to True if sheet is unshared


@patch("subprocess.run")
@patch("requests.get")
def test_download(mock_get, mock_subprocess_run):
    """Test download method

    Mocks subprocess.run to test the download method without actually making
    a network call or downloading any files.

    Mock requests.get to reduce access to Google Sheet.
    """
    mock_get.return_value.ok = True
    url = TEST_SHARED_URL
    sheet = Sheet(url=url, filename="output.csv", description="Test")

    # Call download
    sheet.download()

    # Ensure subprocess.run was called with the correct command
    cmd = ["wget", "--quiet", "-O", "output.csv", sheet.export_url]
    cmd = [str(c) for c in cmd if c]
    mock_subprocess_run.assert_called_with(cmd, check=True)
